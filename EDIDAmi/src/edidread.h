/*
  read VGA DDC via I2C Sensors library
  
  (C) 2018 Henryk Richter

  This program tries to identify VGA 

*/
#ifndef _INC_EDIDREAD_H
#define _INC_EDIDREAD_H

int i2c_readedid( unsigned char *ddcarray, long size, unsigned long flags );

#endif /* _INC_EDIDREAD_H */
