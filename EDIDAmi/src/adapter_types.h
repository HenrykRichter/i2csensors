#ifndef _INC_ADAPTER_TYPES_H
#define _INC_ADAPTER_TYPES_H

#define _X_EXPORT
#define _X_ATTRIBUTE_PRINTF(a,b)

#ifndef NULL
#define NULL (0)
#endif

#define FALSE 0
#define TRUE 1

#define X_INFO 0
#define X_WARNING 1
#define X_ERROR 2

#ifndef MAXINT
#define MAXINT 0x7fffffff
#endif

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(*a))

#define min(_a_,_b_) ( (_a_) > (_b_) ) ? (_b_) : (_a_)
#define max(_a_,_b_) ( (_a_) > (_b_) ) ? (_a_) : (_b_)
#define bits_to_bytes(_a_) (((_a_)+7)>>3)

int XNFasprintf(char **ret, const char *format, ...);
#define xnfcalloc calloc
#define xnfrealloc realloc
#define xstrdup strdup
#define xnfstrdup strdup
#define xnfalloc malloc

typedef short CARD16;
typedef int CARD32;

typedef int Bool;
typedef long INT32;
typedef int *memType;

#if !defined(_STDINT_H) && !defined(__BIT_TYPES_DEFINED__)
typedef unsigned int uint32_t;
#endif

/* THIS IS WRONG, very WRONG. Quick`n`Dirty fix for my old ADE install */
#ifndef uint32_t
typedef unsigned int uint32_t;
#endif

/* float rint(float val); */
#ifndef _INTPTR_T_DECLARED
typedef int* intptr_t;
#define _INTPTR_T_DECLARED
#endif
typedef int* PixmapFormatRec;
/*typedef unsigned int Pix24Flags;*/
typedef unsigned int MessageType;
#define MAXFORMATS 32
typedef unsigned int Atom;
typedef unsigned int Rotation;

typedef unsigned int DevPrivateKeyRec;
typedef void* PixmapFormatPtr;
typedef void* XF86OptionPtr;
/*typedef void* XF86ConfMonitorPtr;*/
typedef void* I2CBusPtr;
/* make xf86str.h happy */
typedef unsigned char *PixmapPtr;
struct _OptionInfoRec;
typedef struct _OptionInfoRec OptionInfoRec;
/* typedef void* ScrnInfoPtr; */
typedef void LOCO;
typedef void* ScreenPtr;
/* typedef void* pmEvent; */
typedef void* VisualPtr;
typedef void DevUnion;
/*typedef void *DriverPtr;*/
typedef struct {
 void *conf_modes_lst;
} *XF86ConfigPtr;

typedef struct
{
  void *next;
  void *prev;
} GenericListRec;

typedef struct
{
	GenericListRec list;
	char *ml_identifier;
	int ml_clock;
	int ml_hdisplay;
	int ml_hsyncstart;
	int ml_hsyncend;
	int ml_htotal;
	int ml_vdisplay;
	int ml_vsyncstart;
	int ml_vsyncend;
	int ml_vtotal;
	int ml_vscan;
	int ml_flags;
	int ml_hskew;
	char *ml_comment;
}
XF86ConfModeLineRec, *XF86ConfModeLinePtr;


typedef struct
{
	GenericListRec list;
	char *modes_identifier;
	XF86ConfModeLinePtr mon_modeline_lst;
	char *modes_comment;
}
XF86ConfModesRec, *XF86ConfModesPtr;

typedef struct
{
	GenericListRec list;
	char *ml_modes_str;
	XF86ConfModesPtr ml_modes;
}
XF86ConfModesLinkRec, *XF86ConfModesLinkPtr;


#define CONF_MAX_HSYNC 8
#define CONF_MAX_VREFRESH 8

typedef struct
{
	float hi, lo;
}
parser_range;

typedef struct
{
	GenericListRec list;
	char *mon_identifier;
	char *mon_vendor;
	char *mon_modelname;
	int mon_width;				/* in mm */
	int mon_height;				/* in mm */
	XF86ConfModeLinePtr mon_modeline_lst;
	int mon_n_hsync;
	parser_range mon_hsync[CONF_MAX_HSYNC];
	int mon_n_vrefresh;
	parser_range mon_vrefresh[CONF_MAX_VREFRESH];
	float mon_gamma_red;
	float mon_gamma_green;
	float mon_gamma_blue;
	XF86OptionPtr mon_option_lst;
	XF86ConfModesLinkPtr mon_modes_sect_lst;
	char *mon_comment;
}
XF86ConfMonitorRec, *XF86ConfMonitorPtr;


/* typedef void *XF86ConfModeLinePtr; */

#define RR_Rotate_0 0
#define RR_Rotate_90 1
#define RR_Rotate_180 2
#define RR_Rotate_270 3


#endif /* _INC_ADAPTER_TYPES_H */
