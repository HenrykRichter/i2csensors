EESchema Schematic File Version 4
LIBS:FanController-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Amiga I2C Fan Controller"
Date ""
Rev "0.1"
Comp "buggs / a1k.org"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Bus_ISA_8bit J1
U 1 1 5CAE04E4
P 2750 2750
F 0 "J1" H 2750 4517 50  0000 C CNN
F 1 "Bus_ISA_8bit" H 2750 4426 50  0000 C CNN
F 2 "Connector_PCBEdge:BUS_ISA8" H 2750 2750 50  0001 C CNN
F 3 "https://en.wikipedia.org/wiki/Industry_Standard_Architecture" H 2750 2750 50  0001 C CNN
	1    2750 2750
	1    0    0    -1  
$EndComp
$Comp
L max31760:MAX31760 U1
U 1 1 5CD5CA2B
P 6400 3000
F 0 "U1" H 6425 4103 60  0000 C CNN
F 1 "MAX31760" H 6425 3997 60  0000 C CNN
F 2 "Package_SO:QSOP-16_3.9x4.9mm_P0.635mm" H 6350 4150 60  0001 C CNN
F 3 "" H 6350 4150 60  0001 C CNN
	1    6400 3000
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LD1117S33TR_SOT223 U2
U 1 1 5CD5E24B
P 5500 900
F 0 "U2" H 5500 1142 50  0000 C CNN
F 1 "LD1117S33TR_SOT223" H 5500 1051 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 5500 1100 50  0001 C CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00000544.pdf" H 5600 650 50  0001 C CNN
	1    5500 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 900  4900 900 
Wire Wire Line
	1600 1450 2050 1450
$Comp
L power:+3.3V #PWR0101
U 1 1 5CD60F40
P 6100 900
F 0 "#PWR0101" H 6100 750 50  0001 C CNN
F 1 "+3.3V" H 6115 1073 50  0000 C CNN
F 2 "" H 6100 900 50  0001 C CNN
F 3 "" H 6100 900 50  0001 C CNN
	1    6100 900 
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0102
U 1 1 5CD61C2E
P 6400 1750
F 0 "#PWR0102" H 6400 1600 50  0001 C CNN
F 1 "+3.3V" H 6415 1923 50  0000 C CNN
F 2 "" H 6400 1750 50  0001 C CNN
F 3 "" H 6400 1750 50  0001 C CNN
	1    6400 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 900  5900 900 
$Comp
L Device:C C1
U 1 1 5CD63244
P 4900 1050
F 0 "C1" H 5015 1096 50  0000 L CNN
F 1 "10uF" H 5015 1005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4938 900 50  0001 C CNN
F 3 "~" H 4900 1050 50  0001 C CNN
	1    4900 1050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5CD64052
P 5900 1050
F 0 "C3" H 6015 1096 50  0000 L CNN
F 1 "10uF" H 6015 1005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5938 900 50  0001 C CNN
F 3 "~" H 5900 1050 50  0001 C CNN
	1    5900 1050
	1    0    0    -1  
$EndComp
Connection ~ 5900 900 
Wire Wire Line
	5900 900  5800 900 
$Comp
L power:GND #PWR0103
U 1 1 5CD6491F
P 4900 1200
F 0 "#PWR0103" H 4900 950 50  0001 C CNN
F 1 "GND" H 4905 1027 50  0000 C CNN
F 2 "" H 4900 1200 50  0001 C CNN
F 3 "" H 4900 1200 50  0001 C CNN
	1    4900 1200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5CD64F17
P 5500 1200
F 0 "#PWR0104" H 5500 950 50  0001 C CNN
F 1 "GND" H 5505 1027 50  0000 C CNN
F 2 "" H 5500 1200 50  0001 C CNN
F 3 "" H 5500 1200 50  0001 C CNN
	1    5500 1200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5CD651CE
P 5900 1200
F 0 "#PWR0105" H 5900 950 50  0001 C CNN
F 1 "GND" H 5905 1027 50  0000 C CNN
F 2 "" H 5900 1200 50  0001 C CNN
F 3 "" H 5900 1200 50  0001 C CNN
	1    5900 1200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5CD654DF
P 1800 1250
F 0 "#PWR0106" H 1800 1000 50  0001 C CNN
F 1 "GND" H 1805 1077 50  0000 C CNN
F 2 "" H 1800 1250 50  0001 C CNN
F 3 "" H 1800 1250 50  0001 C CNN
	1    1800 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 1250 1800 1250
Wire Wire Line
	6400 2100 6400 1950
$Comp
L power:GND #PWR0107
U 1 1 5CD68B35
P 6750 4050
F 0 "#PWR0107" H 6750 3800 50  0001 C CNN
F 1 "GND" H 6755 3877 50  0000 C CNN
F 2 "" H 6750 4050 50  0001 C CNN
F 3 "" H 6750 4050 50  0001 C CNN
	1    6750 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5CD690E4
P 6550 4050
F 0 "#PWR0108" H 6550 3800 50  0001 C CNN
F 1 "GND" H 6555 3877 50  0000 C CNN
F 2 "" H 6550 4050 50  0001 C CNN
F 3 "" H 6550 4050 50  0001 C CNN
	1    6550 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5CD69583
P 6350 4050
F 0 "#PWR0109" H 6350 3800 50  0001 C CNN
F 1 "GND" H 6355 3877 50  0000 C CNN
F 2 "" H 6350 4050 50  0001 C CNN
F 3 "" H 6350 4050 50  0001 C CNN
	1    6350 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5CD69879
P 6150 4050
F 0 "#PWR0110" H 6150 3800 50  0001 C CNN
F 1 "GND" H 6155 3877 50  0000 C CNN
F 2 "" H 6150 4050 50  0001 C CNN
F 3 "" H 6150 4050 50  0001 C CNN
	1    6150 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5CD69DDE
P 5250 2350
F 0 "R3" H 5180 2304 50  0000 R CNN
F 1 "10k" H 5180 2395 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5180 2350 50  0001 C CNN
F 3 "~" H 5250 2350 50  0001 C CNN
	1    5250 2350
	-1   0    0    1   
$EndComp
$Comp
L Device:R R2
U 1 1 5CD6AE66
P 5000 2350
F 0 "R2" H 4930 2304 50  0000 R CNN
F 1 "10k" H 4930 2395 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4930 2350 50  0001 C CNN
F 3 "~" H 5000 2350 50  0001 C CNN
	1    5000 2350
	-1   0    0    1   
$EndComp
$Comp
L Device:R R1
U 1 1 5CD6B358
P 4750 2350
F 0 "R1" H 4680 2304 50  0000 R CNN
F 1 "10k" H 4680 2395 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4680 2350 50  0001 C CNN
F 3 "~" H 4750 2350 50  0001 C CNN
	1    4750 2350
	-1   0    0    1   
$EndComp
Wire Wire Line
	5650 2500 5250 2500
Wire Wire Line
	5650 2650 5000 2650
Wire Wire Line
	5000 2650 5000 2500
Wire Wire Line
	5650 2800 4750 2800
Wire Wire Line
	4750 2800 4750 2500
$Comp
L Device:R R4
U 1 1 5CD6BEB9
P 7400 2200
F 0 "R4" H 7330 2154 50  0000 R CNN
F 1 "4.7k" H 7330 2245 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7330 2200 50  0001 C CNN
F 3 "~" H 7400 2200 50  0001 C CNN
	1    7400 2200
	-1   0    0    1   
$EndComp
$Comp
L Device:R R5
U 1 1 5CD6C270
P 7700 2200
F 0 "R5" H 7630 2154 50  0000 R CNN
F 1 "4.7k" H 7630 2245 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7630 2200 50  0001 C CNN
F 3 "~" H 7700 2200 50  0001 C CNN
	1    7700 2200
	-1   0    0    1   
$EndComp
Wire Wire Line
	7200 2500 7400 2500
Wire Wire Line
	7400 2500 7400 2350
Wire Wire Line
	7700 2650 7700 2350
Wire Wire Line
	4750 2200 5000 2200
Wire Wire Line
	5000 2200 5250 2200
Connection ~ 5000 2200
Wire Wire Line
	5000 2200 5000 1950
Wire Wire Line
	5000 1950 6400 1950
Connection ~ 6400 1950
Wire Wire Line
	6400 1950 6400 1750
Wire Wire Line
	7400 2050 7400 1950
Wire Wire Line
	7400 1950 6400 1950
Wire Wire Line
	7700 2050 7700 1950
Wire Wire Line
	7700 1950 7400 1950
Connection ~ 7400 1950
$Comp
L Device:C C4
U 1 1 5CD70469
P 6650 1050
F 0 "C4" H 6765 1096 50  0000 L CNN
F 1 "100nF" H 6765 1005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6688 900 50  0001 C CNN
F 3 "~" H 6650 1050 50  0001 C CNN
	1    6650 1050
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0111
U 1 1 5CD708A8
P 6650 900
F 0 "#PWR0111" H 6650 750 50  0001 C CNN
F 1 "+3.3V" H 6665 1073 50  0000 C CNN
F 2 "" H 6650 900 50  0001 C CNN
F 3 "" H 6650 900 50  0001 C CNN
	1    6650 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5CD71844
P 6650 1200
F 0 "#PWR0112" H 6650 950 50  0001 C CNN
F 1 "GND" H 6655 1027 50  0000 C CNN
F 2 "" H 6650 1200 50  0001 C CNN
F 3 "" H 6650 1200 50  0001 C CNN
	1    6650 1200
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR0114
U 1 1 5CD738EB
P 1250 1850
F 0 "#PWR0114" H 1250 1950 50  0001 C CNN
F 1 "-12V" H 1265 2023 50  0000 C CNN
F 2 "" H 1250 1850 50  0001 C CNN
F 3 "" H 1250 1850 50  0001 C CNN
	1    1250 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 1650 1450 1650
Wire Wire Line
	2050 1850 1250 1850
Text GLabel 4950 3100 0    50   Input ~ 0
SDA
Text GLabel 4950 3250 0    50   Input ~ 0
SCL
Wire Wire Line
	4950 3100 5650 3100
Wire Wire Line
	5650 3250 4950 3250
Text GLabel 8150 1000 0    50   Input ~ 0
SDA
Text GLabel 8150 1100 0    50   Input ~ 0
SCL
$Comp
L Connector_Generic:Conn_01x04 JI2C1
U 1 1 5CD783AB
P 8350 900
F 0 "JI2C1" H 8430 892 50  0000 L CNN
F 1 "Conn_01x04" H 8430 801 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Horizontal" H 8350 900 50  0001 C CNN
F 3 "~" H 8350 900 50  0001 C CNN
	1    8350 900 
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5CD7D169
P 5250 3650
F 0 "C2" H 5365 3696 50  0000 L CNN
F 1 "2.2nF" H 5365 3605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5288 3500 50  0001 C CNN
F 3 "~" H 5250 3650 50  0001 C CNN
	1    5250 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3700 5650 3800
Wire Wire Line
	5650 3800 5250 3800
Wire Wire Line
	5650 3550 5650 3500
Wire Wire Line
	5650 3500 5250 3500
Wire Wire Line
	5250 3500 4750 3500
Connection ~ 5250 3500
Wire Wire Line
	5250 3800 4750 3800
Connection ~ 5250 3800
$Comp
L Connector_Generic:Conn_01x02 JTemp1
U 1 1 5CD7B62A
P 4400 3700
F 0 "JTemp1" H 4318 3375 50  0000 C CNN
F 1 "Conn_01x02" H 4318 3466 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Horizontal" H 4400 3700 50  0001 C CNN
F 3 "~" H 4400 3700 50  0001 C CNN
	1    4400 3700
	-1   0    0    1   
$EndComp
Wire Wire Line
	4750 3500 4750 3600
Wire Wire Line
	4750 3600 4600 3600
Wire Wire Line
	4750 3800 4750 3700
Wire Wire Line
	4750 3700 4600 3700
$Comp
L power:+5V #PWR0115
U 1 1 5CD8794F
P 1600 1450
F 0 "#PWR0115" H 1600 1300 50  0001 C CNN
F 1 "+5V" H 1615 1623 50  0000 C CNN
F 2 "" H 1600 1450 50  0001 C CNN
F 3 "" H 1600 1450 50  0001 C CNN
	1    1600 1450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0116
U 1 1 5CD88480
P 4700 900
F 0 "#PWR0116" H 4700 750 50  0001 C CNN
F 1 "+5V" H 4715 1073 50  0000 C CNN
F 2 "" H 4700 900 50  0001 C CNN
F 3 "" H 4700 900 50  0001 C CNN
	1    4700 900 
	1    0    0    -1  
$EndComp
Connection ~ 4900 900 
Wire Wire Line
	4900 900  4700 900 
$Comp
L power:GND #PWR0118
U 1 1 5CD8A61B
P 7800 900
F 0 "#PWR0118" H 7800 650 50  0001 C CNN
F 1 "GND" H 7805 727 50  0000 C CNN
F 2 "" H 7800 900 50  0001 C CNN
F 3 "" H 7800 900 50  0001 C CNN
	1    7800 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 900  7800 900 
$Comp
L Connector_Generic:Conn_01x04 JFan1
U 1 1 5CD8C6CB
P 9550 1700
F 0 "JFan1" H 9630 1692 50  0000 L CNN
F 1 "Conn_01x04" H 9630 1601 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Horizontal" H 9550 1700 50  0001 C CNN
F 3 "~" H 9550 1700 50  0001 C CNN
	1    9550 1700
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 JFan2
U 1 1 5CD8E72C
P 9550 2400
F 0 "JFan2" H 9630 2392 50  0000 L CNN
F 1 "Conn_01x04" H 9630 2301 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Horizontal" H 9550 2400 50  0001 C CNN
F 3 "~" H 9550 2400 50  0001 C CNN
	1    9550 2400
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0117
U 1 1 5CD8F2E3
P 1100 2050
F 0 "#PWR0117" H 1100 1900 50  0001 C CNN
F 1 "+12V" H 1115 2223 50  0000 C CNN
F 2 "" H 1100 2050 50  0001 C CNN
F 3 "" H 1100 2050 50  0001 C CNN
	1    1100 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 2050 1100 2050
$Comp
L power:GND #PWR0119
U 1 1 5CD917F2
P 9150 2600
F 0 "#PWR0119" H 9150 2350 50  0001 C CNN
F 1 "GND" H 9155 2427 50  0000 C CNN
F 2 "" H 9150 2600 50  0001 C CNN
F 3 "" H 9150 2600 50  0001 C CNN
	1    9150 2600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0120
U 1 1 5CD92EA6
P 9150 1900
F 0 "#PWR0120" H 9150 1650 50  0001 C CNN
F 1 "GND" H 9155 1727 50  0000 C CNN
F 2 "" H 9150 1900 50  0001 C CNN
F 3 "" H 9150 1900 50  0001 C CNN
	1    9150 1900
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0121
U 1 1 5CD9329D
P 8800 2300
F 0 "#PWR0121" H 8800 2150 50  0001 C CNN
F 1 "+12V" H 8815 2473 50  0000 C CNN
F 2 "" H 8800 2300 50  0001 C CNN
F 3 "" H 8800 2300 50  0001 C CNN
	1    8800 2300
	1    0    0    -1  
$EndComp
Text GLabel 7600 3100 2    50   Input ~ 0
PWM1
$Comp
L Device:R R9
U 1 1 5CD95AB4
P 7400 3000
F 0 "R9" V 7607 3000 50  0000 C CNN
F 1 "4.7k" V 7516 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7330 3000 50  0001 C CNN
F 3 "~" H 7400 3000 50  0001 C CNN
	1    7400 3000
	0    -1   -1   0   
$EndComp
Text GLabel 9200 1600 0    50   Input ~ 0
PWM1
Text GLabel 9200 2300 0    50   Input ~ 0
PWM1
$Comp
L power:+12V #PWR0122
U 1 1 5CD98B5B
P 8800 1600
F 0 "#PWR0122" H 8800 1450 50  0001 C CNN
F 1 "+12V" H 8815 1773 50  0000 C CNN
F 2 "" H 8800 1600 50  0001 C CNN
F 3 "" H 8800 1600 50  0001 C CNN
	1    8800 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 2300 9350 2300
Wire Wire Line
	9200 1600 9350 1600
$Comp
L power:+3.3V #PWR0123
U 1 1 5CD9E508
P 7700 3000
F 0 "#PWR0123" H 7700 2850 50  0001 C CNN
F 1 "+3.3V" H 7715 3173 50  0000 C CNN
F 2 "" H 7700 3000 50  0001 C CNN
F 3 "" H 7700 3000 50  0001 C CNN
	1    7700 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 3100 7250 3100
Text GLabel 7450 2500 2    50   Input ~ 0
Fan1
Text GLabel 7750 2650 2    50   Input ~ 0
Fan2
Wire Wire Line
	7750 2650 7700 2650
Connection ~ 7700 2650
Wire Wire Line
	7700 2650 7200 2650
Wire Wire Line
	7450 2500 7400 2500
Connection ~ 7400 2500
Text GLabel 9150 1700 0    50   Input ~ 0
Fan1
Text GLabel 9150 2400 0    50   Input ~ 0
Fan2
Wire Wire Line
	9350 1700 9150 1700
Wire Wire Line
	9350 2400 9150 2400
Wire Wire Line
	7700 3000 7550 3000
Wire Wire Line
	7250 3000 7250 3100
Connection ~ 7250 3100
Wire Wire Line
	7250 3100 7200 3100
$Comp
L max31760:MAX31760 U3
U 1 1 5CDCE51C
P 6450 5600
F 0 "U3" H 6000 6350 60  0000 C CNN
F 1 "MAX31760" H 6500 5650 60  0000 C CNN
F 2 "Package_SO:QSOP-16_3.9x4.9mm_P0.635mm" H 6400 6750 60  0001 C CNN
F 3 "" H 6400 6750 60  0001 C CNN
	1    6450 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5CDCF54A
P 5250 6250
F 0 "C5" H 5365 6296 50  0000 L CNN
F 1 "2.2nF" H 5365 6205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5288 6100 50  0001 C CNN
F 3 "~" H 5250 6250 50  0001 C CNN
	1    5250 6250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 JTemp2
U 1 1 5CDCFFBC
P 4550 6300
F 0 "JTemp2" H 4468 5975 50  0000 C CNN
F 1 "Conn_01x02" H 4468 6066 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Horizontal" H 4550 6300 50  0001 C CNN
F 3 "~" H 4550 6300 50  0001 C CNN
	1    4550 6300
	-1   0    0    1   
$EndComp
Text GLabel 5450 5700 0    50   Input ~ 0
SDA
Text GLabel 5450 5850 0    50   Input ~ 0
SCL
$Comp
L Device:R R8
U 1 1 5CDD0923
P 5400 4950
F 0 "R8" H 5330 4904 50  0000 R CNN
F 1 "10k" H 5330 4995 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5330 4950 50  0001 C CNN
F 3 "~" H 5400 4950 50  0001 C CNN
	1    5400 4950
	-1   0    0    1   
$EndComp
$Comp
L Device:R R7
U 1 1 5CDD1451
P 5100 4950
F 0 "R7" H 5030 4904 50  0000 R CNN
F 1 "10k" H 5030 4995 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5030 4950 50  0001 C CNN
F 3 "~" H 5100 4950 50  0001 C CNN
	1    5100 4950
	-1   0    0    1   
$EndComp
$Comp
L Device:R R6
U 1 1 5CDD1982
P 4800 4950
F 0 "R6" H 4730 4904 50  0000 R CNN
F 1 "10k" H 4730 4995 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4730 4950 50  0001 C CNN
F 3 "~" H 4800 4950 50  0001 C CNN
	1    4800 4950
	-1   0    0    1   
$EndComp
$Comp
L Device:R R10
U 1 1 5CDD1DCC
P 7450 4950
F 0 "R10" H 7380 4904 50  0000 R CNN
F 1 "4.7k" H 7380 4995 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7380 4950 50  0001 C CNN
F 3 "~" H 7450 4950 50  0001 C CNN
	1    7450 4950
	-1   0    0    1   
$EndComp
$Comp
L Device:R R11
U 1 1 5CDD24B6
P 7950 4950
F 0 "R11" H 7880 4904 50  0000 R CNN
F 1 "4.7k" H 7880 4995 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7880 4950 50  0001 C CNN
F 3 "~" H 7950 4950 50  0001 C CNN
	1    7950 4950
	-1   0    0    1   
$EndComp
$Comp
L Device:R R12
U 1 1 5CDD2969
P 8350 4950
F 0 "R12" H 8280 4904 50  0000 R CNN
F 1 "4.7k" H 8280 4995 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8280 4950 50  0001 C CNN
F 3 "~" H 8350 4950 50  0001 C CNN
	1    8350 4950
	-1   0    0    1   
$EndComp
Wire Wire Line
	7250 5100 7450 5100
Text GLabel 7600 5100 2    50   Input ~ 0
Fan3
Text GLabel 8050 5250 2    50   Input ~ 0
Fan4
Text GLabel 8500 5700 2    50   Input ~ 0
PWM2
Wire Wire Line
	7600 5100 7450 5100
Connection ~ 7450 5100
Wire Wire Line
	7250 5250 7950 5250
Wire Wire Line
	7950 5100 7950 5250
Connection ~ 7950 5250
Wire Wire Line
	7950 5250 8050 5250
Wire Wire Line
	7250 5700 8350 5700
Wire Wire Line
	8350 5700 8350 5100
Wire Wire Line
	8500 5700 8350 5700
Connection ~ 8350 5700
$Comp
L power:+3.3V #PWR0124
U 1 1 5CDDAF71
P 4800 4800
F 0 "#PWR0124" H 4800 4650 50  0001 C CNN
F 1 "+3.3V" H 4815 4973 50  0000 C CNN
F 2 "" H 4800 4800 50  0001 C CNN
F 3 "" H 4800 4800 50  0001 C CNN
	1    4800 4800
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0125
U 1 1 5CDDB80C
P 5100 4800
F 0 "#PWR0125" H 5100 4650 50  0001 C CNN
F 1 "+3.3V" H 5115 4973 50  0000 C CNN
F 2 "" H 5100 4800 50  0001 C CNN
F 3 "" H 5100 4800 50  0001 C CNN
	1    5100 4800
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0126
U 1 1 5CDDBB5A
P 5400 4800
F 0 "#PWR0126" H 5400 4650 50  0001 C CNN
F 1 "+3.3V" H 5415 4973 50  0000 C CNN
F 2 "" H 5400 4800 50  0001 C CNN
F 3 "" H 5400 4800 50  0001 C CNN
	1    5400 4800
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0127
U 1 1 5CDDBE7A
P 6450 4700
F 0 "#PWR0127" H 6450 4550 50  0001 C CNN
F 1 "+3.3V" H 6465 4873 50  0000 C CNN
F 2 "" H 6450 4700 50  0001 C CNN
F 3 "" H 6450 4700 50  0001 C CNN
	1    6450 4700
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0128
U 1 1 5CDDCBAD
P 7450 4800
F 0 "#PWR0128" H 7450 4650 50  0001 C CNN
F 1 "+3.3V" H 7465 4973 50  0000 C CNN
F 2 "" H 7450 4800 50  0001 C CNN
F 3 "" H 7450 4800 50  0001 C CNN
	1    7450 4800
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0129
U 1 1 5CDDD1DB
P 7950 4800
F 0 "#PWR0129" H 7950 4650 50  0001 C CNN
F 1 "+3.3V" H 7965 4973 50  0000 C CNN
F 2 "" H 7950 4800 50  0001 C CNN
F 3 "" H 7950 4800 50  0001 C CNN
	1    7950 4800
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0130
U 1 1 5CDDD6F5
P 8350 4800
F 0 "#PWR0130" H 8350 4650 50  0001 C CNN
F 1 "+3.3V" H 8365 4973 50  0000 C CNN
F 2 "" H 8350 4800 50  0001 C CNN
F 3 "" H 8350 4800 50  0001 C CNN
	1    8350 4800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0131
U 1 1 5CDDDD51
P 6400 6650
F 0 "#PWR0131" H 6400 6400 50  0001 C CNN
F 1 "GND" H 6405 6477 50  0000 C CNN
F 2 "" H 6400 6650 50  0001 C CNN
F 3 "" H 6400 6650 50  0001 C CNN
	1    6400 6650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0132
U 1 1 5CDDE256
P 6800 6650
F 0 "#PWR0132" H 6800 6400 50  0001 C CNN
F 1 "GND" H 6805 6477 50  0000 C CNN
F 2 "" H 6800 6650 50  0001 C CNN
F 3 "" H 6800 6650 50  0001 C CNN
	1    6800 6650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0133
U 1 1 5CDDE68F
P 6600 6650
F 0 "#PWR0133" H 6600 6400 50  0001 C CNN
F 1 "GND" H 6605 6477 50  0000 C CNN
F 2 "" H 6600 6650 50  0001 C CNN
F 3 "" H 6600 6650 50  0001 C CNN
	1    6600 6650
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0134
U 1 1 5CDDEC2E
P 5950 6850
F 0 "#PWR0134" H 5950 6700 50  0001 C CNN
F 1 "+3.3V" H 5965 7023 50  0000 C CNN
F 2 "" H 5950 6850 50  0001 C CNN
F 3 "" H 5950 6850 50  0001 C CNN
	1    5950 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 6850 6200 6850
Wire Wire Line
	6200 6850 6200 6650
Wire Wire Line
	5700 6300 5700 6400
Wire Wire Line
	5700 6400 5250 6400
Wire Wire Line
	5700 6150 5700 6100
Wire Wire Line
	5700 6100 5250 6100
Wire Wire Line
	5250 6100 4750 6100
Wire Wire Line
	4750 6100 4750 6200
Connection ~ 5250 6100
Wire Wire Line
	4750 6300 4750 6400
Wire Wire Line
	4750 6400 5250 6400
Connection ~ 5250 6400
Wire Wire Line
	5700 5700 5450 5700
Wire Wire Line
	5700 5850 5450 5850
Wire Wire Line
	5700 5100 5400 5100
Wire Wire Line
	5700 5250 5100 5250
Wire Wire Line
	5100 5250 5100 5100
Wire Wire Line
	5700 5400 4800 5400
Wire Wire Line
	4800 5400 4800 5100
$Comp
L Connector_Generic:Conn_01x04 JFan4
U 1 1 5CDF29F9
P 9700 5850
F 0 "JFan4" H 9780 5842 50  0000 L CNN
F 1 "Conn_01x04" H 9780 5751 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Horizontal" H 9700 5850 50  0001 C CNN
F 3 "~" H 9700 5850 50  0001 C CNN
	1    9700 5850
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 JFan3
U 1 1 5CDF3389
P 9700 5100
F 0 "JFan3" H 9780 5092 50  0000 L CNN
F 1 "Conn_01x04" H 9780 5001 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Horizontal" H 9700 5100 50  0001 C CNN
F 3 "~" H 9700 5100 50  0001 C CNN
	1    9700 5100
	1    0    0    -1  
$EndComp
Text GLabel 9500 5000 0    50   Input ~ 0
PWM2
Text GLabel 9500 5750 0    50   Input ~ 0
PWM2
Text GLabel 9500 5100 0    50   Input ~ 0
Fan3
Text GLabel 9500 5850 0    50   Input ~ 0
Fan4
$Comp
L power:+12V #PWR0135
U 1 1 5CDF428D
P 9100 5200
F 0 "#PWR0135" H 9100 5050 50  0001 C CNN
F 1 "+12V" H 9115 5373 50  0000 C CNN
F 2 "" H 9100 5200 50  0001 C CNN
F 3 "" H 9100 5200 50  0001 C CNN
	1    9100 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0136
U 1 1 5CDF490E
P 9100 5300
F 0 "#PWR0136" H 9100 5050 50  0001 C CNN
F 1 "GND" H 9105 5127 50  0000 C CNN
F 2 "" H 9100 5300 50  0001 C CNN
F 3 "" H 9100 5300 50  0001 C CNN
	1    9100 5300
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0137
U 1 1 5CDF98E8
P 9100 5950
F 0 "#PWR0137" H 9100 5800 50  0001 C CNN
F 1 "+12V" H 9115 6123 50  0000 C CNN
F 2 "" H 9100 5950 50  0001 C CNN
F 3 "" H 9100 5950 50  0001 C CNN
	1    9100 5950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0138
U 1 1 5CDFA2FB
P 9100 6050
F 0 "#PWR0138" H 9100 5800 50  0001 C CNN
F 1 "GND" H 9105 5877 50  0000 C CNN
F 2 "" H 9100 6050 50  0001 C CNN
F 3 "" H 9100 6050 50  0001 C CNN
	1    9100 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 5950 9500 5950
Wire Wire Line
	9500 6050 9100 6050
Wire Wire Line
	9100 5300 9500 5300
Wire Wire Line
	9100 5200 9500 5200
$Comp
L power:GND #PWR0139
U 1 1 5CE17D62
P 1800 4250
F 0 "#PWR0139" H 1800 4000 50  0001 C CNN
F 1 "GND" H 1805 4077 50  0000 C CNN
F 2 "" H 1800 4250 50  0001 C CNN
F 3 "" H 1800 4250 50  0001 C CNN
	1    1800 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 4250 1800 4250
$Comp
L Device:C C6
U 1 1 5CD6A609
P 7150 1050
F 0 "C6" H 7265 1096 50  0000 L CNN
F 1 "100nF" H 7265 1005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7188 900 50  0001 C CNN
F 3 "~" H 7150 1050 50  0001 C CNN
	1    7150 1050
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0140
U 1 1 5CD6DFAD
P 7150 900
F 0 "#PWR0140" H 7150 750 50  0001 C CNN
F 1 "+3.3V" H 7165 1073 50  0000 C CNN
F 2 "" H 7150 900 50  0001 C CNN
F 3 "" H 7150 900 50  0001 C CNN
	1    7150 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0141
U 1 1 5CD6E39E
P 7150 1200
F 0 "#PWR0141" H 7150 950 50  0001 C CNN
F 1 "GND" H 7155 1027 50  0000 C CNN
F 2 "" H 7150 1200 50  0001 C CNN
F 3 "" H 7150 1200 50  0001 C CNN
	1    7150 1200
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_3_Open JP1
U 1 1 5CD947AF
P 8800 1800
F 0 "JP1" V 8846 1867 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 8755 1867 50  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_Pad1.0x1.5mm" H 8800 1800 50  0001 C CNN
F 3 "~" H 8800 1800 50  0001 C CNN
	1    8800 1800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9350 1800 8950 1800
Wire Wire Line
	9350 1900 9150 1900
$Comp
L power:+5V #PWR0142
U 1 1 5CDACEAE
P 8600 1600
F 0 "#PWR0142" H 8600 1450 50  0001 C CNN
F 1 "+5V" H 8615 1773 50  0000 C CNN
F 2 "" H 8600 1600 50  0001 C CNN
F 3 "" H 8600 1600 50  0001 C CNN
	1    8600 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 1600 8600 2000
Wire Wire Line
	8600 2000 8800 2000
$Comp
L Jumper:SolderJumper_3_Open JP2
U 1 1 5CDB8A48
P 8800 2500
F 0 "JP2" V 8846 2567 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 8755 2567 50  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_Pad1.0x1.5mm" H 8800 2500 50  0001 C CNN
F 3 "~" H 8800 2500 50  0001 C CNN
	1    8800 2500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9350 2500 8950 2500
$Comp
L power:+5V #PWR0143
U 1 1 5CDBC18A
P 8600 2300
F 0 "#PWR0143" H 8600 2150 50  0001 C CNN
F 1 "+5V" H 8615 2473 50  0000 C CNN
F 2 "" H 8600 2300 50  0001 C CNN
F 3 "" H 8600 2300 50  0001 C CNN
	1    8600 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 2300 8600 2700
Wire Wire Line
	8600 2700 8800 2700
Wire Wire Line
	9350 2600 9150 2600
$Comp
L Sensor:LTC2990 U4
U 1 1 5CD77A5C
P 2150 6300
F 0 "U4" H 1800 5800 50  0000 C CNN
F 1 "LTC2990" H 2150 6300 50  0000 C CNN
F 2 "Package_SO:MSOP-10_3x3mm_P0.5mm" H 2150 6300 50  0001 C CNN
F 3 "http://cds.linear.com/docs/en/datasheet/2990fe.pdf" H 1550 6800 50  0001 C CNN
	1    2150 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0144
U 1 1 5CD7A2DE
P 2150 6900
F 0 "#PWR0144" H 2150 6650 50  0001 C CNN
F 1 "GND" H 2155 6727 50  0000 C CNN
F 2 "" H 2150 6900 50  0001 C CNN
F 3 "" H 2150 6900 50  0001 C CNN
	1    2150 6900
	1    0    0    -1  
$EndComp
Text GLabel 2750 6200 2    50   Input ~ 0
SDA
Text GLabel 2750 6300 2    50   Input ~ 0
SCL
$Comp
L power:GND #PWR0146
U 1 1 5CD7C467
P 2750 6600
F 0 "#PWR0146" H 2750 6350 50  0001 C CNN
F 1 "GND" H 2755 6427 50  0000 C CNN
F 2 "" H 2750 6600 50  0001 C CNN
F 3 "" H 2750 6600 50  0001 C CNN
	1    2750 6600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 6500 2750 6500
$Comp
L power:GND #PWR0148
U 1 1 5CD8028C
P 2250 5500
F 0 "#PWR0148" H 2250 5250 50  0001 C CNN
F 1 "GND" H 2255 5327 50  0000 C CNN
F 2 "" H 2250 5500 50  0001 C CNN
F 3 "" H 2250 5500 50  0001 C CNN
	1    2250 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 5500 2150 5500
Wire Wire Line
	2150 5500 2150 5700
$Comp
L Device:R R16
U 1 1 5CD83AED
P 2150 5350
F 0 "R16" H 2080 5304 50  0000 R CNN
F 1 "1k" H 2080 5395 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2080 5350 50  0001 C CNN
F 3 "~" H 2150 5350 50  0001 C CNN
	1    2150 5350
	-1   0    0    1   
$EndComp
Connection ~ 2150 5500
Wire Wire Line
	2150 5200 1850 5200
Wire Wire Line
	1850 5200 1850 5700
$Comp
L power:-12V #PWR0149
U 1 1 5CD874CA
P 1200 5200
F 0 "#PWR0149" H 1200 5300 50  0001 C CNN
F 1 "-12V" H 1215 5373 50  0000 C CNN
F 2 "" H 1200 5200 50  0001 C CNN
F 3 "" H 1200 5200 50  0001 C CNN
	1    1200 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R15
U 1 1 5CD882C2
P 1700 5200
F 0 "R15" H 1630 5154 50  0000 R CNN
F 1 "56k" H 1630 5245 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1630 5200 50  0001 C CNN
F 3 "~" H 1700 5200 50  0001 C CNN
	1    1700 5200
	0    -1   -1   0   
$EndComp
Connection ~ 1850 5200
Wire Wire Line
	1200 5200 1550 5200
$Comp
L power:GND #PWR0150
U 1 1 5CD8C368
P 1250 6500
F 0 "#PWR0150" H 1250 6250 50  0001 C CNN
F 1 "GND" H 1255 6327 50  0000 C CNN
F 2 "" H 1250 6500 50  0001 C CNN
F 3 "" H 1250 6500 50  0001 C CNN
	1    1250 6500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R14
U 1 1 5CD8FE83
P 1250 6300
F 0 "R14" H 1180 6254 50  0000 R CNN
F 1 "2.2k" H 1180 6345 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1180 6300 50  0001 C CNN
F 3 "~" H 1250 6300 50  0001 C CNN
	1    1250 6300
	-1   0    0    1   
$EndComp
$Comp
L power:-5V #PWR0113
U 1 1 5CD72079
P 1450 1650
F 0 "#PWR0113" H 1450 1750 50  0001 C CNN
F 1 "-5V" H 1465 1823 50  0000 C CNN
F 2 "" H 1450 1650 50  0001 C CNN
F 3 "" H 1450 1650 50  0001 C CNN
	1    1450 1650
	1    0    0    -1  
$EndComp
$Comp
L power:-5V #PWR0151
U 1 1 5CD9BD5D
P 1250 5800
F 0 "#PWR0151" H 1250 5900 50  0001 C CNN
F 1 "-5V" H 1265 5973 50  0000 C CNN
F 2 "" H 1250 5800 50  0001 C CNN
F 3 "" H 1250 5800 50  0001 C CNN
	1    1250 5800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R13
U 1 1 5CD976CF
P 1250 5950
F 0 "R13" H 1180 5904 50  0000 R CNN
F 1 "56k" H 1180 5995 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1180 5950 50  0001 C CNN
F 3 "~" H 1250 5950 50  0001 C CNN
	1    1250 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 6500 1250 6450
Wire Wire Line
	1250 6150 1250 6100
Wire Wire Line
	1550 6500 1250 6500
Connection ~ 1250 6500
Wire Wire Line
	1550 6100 1250 6100
Connection ~ 1250 6100
$Comp
L power:+3.3V #PWR0145
U 1 1 5CDE5EF1
P 2450 5700
F 0 "#PWR0145" H 2450 5550 50  0001 C CNN
F 1 "+3.3V" H 2465 5873 50  0000 C CNN
F 2 "" H 2450 5700 50  0001 C CNN
F 3 "" H 2450 5700 50  0001 C CNN
	1    2450 5700
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0147
U 1 1 5CDE65A2
P 3050 6500
F 0 "#PWR0147" H 3050 6350 50  0001 C CNN
F 1 "+3.3V" H 3065 6673 50  0000 C CNN
F 2 "" H 3050 6500 50  0001 C CNN
F 3 "" H 3050 6500 50  0001 C CNN
	1    3050 6500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 5CDEE554
P 3350 6300
F 0 "C7" H 3465 6346 50  0000 L CNN
F 1 "100nF" H 3465 6255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3388 6150 50  0001 C CNN
F 3 "~" H 3350 6300 50  0001 C CNN
	1    3350 6300
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0152
U 1 1 5CDEF067
P 3350 6150
F 0 "#PWR0152" H 3350 6000 50  0001 C CNN
F 1 "+3.3V" H 3365 6323 50  0000 C CNN
F 2 "" H 3350 6150 50  0001 C CNN
F 3 "" H 3350 6150 50  0001 C CNN
	1    3350 6150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0153
U 1 1 5CDEF758
P 3350 6450
F 0 "#PWR0153" H 3350 6200 50  0001 C CNN
F 1 "GND" H 3355 6277 50  0000 C CNN
F 2 "" H 3350 6450 50  0001 C CNN
F 3 "" H 3350 6450 50  0001 C CNN
	1    3350 6450
	1    0    0    -1  
$EndComp
$EndSCHEMATC
