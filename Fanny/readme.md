# Fanny 
 
The Fanny fan controller is a small PCB that plugs into
a free ISA slot. It is designed around the MAX31760 chip
that can provide temperature based fan control and rotation
counting for up to two fans. Fanny is supposed to operate
PWM fans, either by internal or external temperature readings.
The board supports one or two independently programmable
MAX31760. This way, up to four fans can be directly connected
and monitored. Additionally, the PCB can accomodate an LTC2990
voltage monitor for -5V and -12V.

This project is mainly intended for classic Amiga computers (2000,3000,4000).

![Cards with minimal and full population](https://gitlab.com/HenrykRichter/i2csensors/raw/master/Fanny/PCB/Pics/Minimal_vs_Two-Way.JPG)

## Hardware

The minimal configuration consists of a single MAX31760, a 3.3V
voltage controller and some passives. The main circuit can be configured
to either 5V or 12V fans by means of two solder jumpers on the backside
of the card. 

While the card is able to monitor regular 3 pin fans for rotation
speed, it is suggested to replace at least the main PSU fan by
a PWM type model.

Configuration and monitoring of the card requires an I2C interface.
Schematics and support software (i2c.library) are available on
Aminet (e.g. docs/hard/i2clib40.lha, docs/hard/icy.lzh). 

Implementation and testing of this project has been done using
a recent clone of the M. B�hmer's ICY card (a1k.org project and 
group order ICYv2) and Matze's A3000 daughterboard featuring a 
CPLD based variant of the ICY controller.

![Backside with 5V/12V solder jumpers](https://gitlab.com/HenrykRichter/i2csensors/raw/master/Fanny/PCB/Pics/Backside_5V_12V.JPG)


## Software

Please note that the default setting for new MAX31760 chips is
to run at full PWM level. An AmigaOS/68k tool to set sensible
trade-off parameters between fan noise and cooling is part of 
this repository. 

You may choose suitable parameters that fit your
specific computer setup in terms of temperature limits and associated
PWM fan speeds. Due to the fact that both your computer
and their installed fans may differ quite a bit from others, 
you need to be prepared for a little hand-tuning in order to get
optimal results.

Monitoring of temperatures and fan RPM is beyond the scope
of the configuration tool. Please refer to the I2CSensors.library. Example
config files for the two MAX31760 (at I2C addresses A0,A2) and LTC2990 
(at I2C address 0x9a) are part of the distribution. 


**DISCLAIMER!!!!**  
**THIS IS A HOBBYIST PROJECT! I'M NOT RESPONSIBLE IF IT DOESN'T WORK OR DESTROYS YOUR VALUABLE DATA OR LEADS TO DAMAGED COMPUTERS!**

## License
All sources published under GNU GENERAL PUBLIC LICENSE V2.0. The PCB and it's design files are licensed as [CC BY NC SA](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.en).
