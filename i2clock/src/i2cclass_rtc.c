/*
  file:   i2cclass_rtc.c

  proposal for I2C class by the example of RTC module support

  author: Henryk Richter <henryk.richter@gmx.net>

  notes:  adding a new vendor/chip: have a look at the
          Philips PCF8583, taglists in here start with the
	  chip and branch to the vendor by means of TAG_MORE

          The global list is i2crtc_chips, where a new chip
	  gets added.

*/
#include "i2cclass_rtc.h"
#include <exec/memory.h>
#include <proto/exec.h>
#include <proto/utility.h>
#include <proto/dos.h>
#include <proto/timer.h>
#define __CONSTLIBBASEDECL__ 
#include <proto/i2c.h>

/* redirect Library pointers */
extern struct ExecBase *SysBase; /* initialized at first Open */
#define UtilityBase i2c_rtcbase->utilitylib
#define I2C_Base i2c_rtcbase->i2clib
#define TimerBase (struct Library *)i2c_rtcbase->timer.tr_node.io_Device

/* string constants */
const char dallas_name[] = "Dallas/Maxim";
const char philips_name[] = "Philips";
const char ricoh_name[] = "Ricoh";

const char ds1307_name[] = "DS1307";
const char ds1629_name[] = "DS1629";
const char ds3231_name[] = "DS3231";
const char pcf8583_name[] = "PCF8583";
const char r2025_name[] = "R2025";

/*-----------------------------------------------------------------------*/
/* static taglists for supported RTC vendors                             */
/*-----------------------------------------------------------------------*/

/* Dallas common */
const struct TagItem i2crtc_dallas[] = {
 { I2CRTC_VENDOR, (ULONG)dallas_name },
 { I2CRTC_VENDORID, I2C_VID_DALLAS },
 { I2C_RTC, 1 }, /* 1 clock */
 { TAG_DONE, 0 }
};

/* Dallas at D0/D1 */
const struct TagItem i2crtc_dallasD0[] = {
 { I2C_RDID, 0xd1 },
 { I2C_WRID, 0xd0 },
 { I2CRTC_READCALL, (ULONG)i2c_RTCReadDS1307 },
 { I2CRTC_WRITECALL, (ULONG)i2c_RTCWriteDS1307 },
 { TAG_MORE, (ULONG)i2crtc_dallas }
};

/* Dallas at 9E/9F */
const struct TagItem i2crtc_dallas9E[] = {
 { I2C_RDID, 0x9f },
 { I2C_WRID, 0x9e },
 { I2CRTC_READCALL, (ULONG)i2c_RTCReadDS1609 },
 { I2CRTC_WRITECALL, (ULONG)i2c_RTCWriteDS1609 },
 { TAG_MORE, (ULONG)i2crtc_dallas }
};


/* Philips at A0/A1 */
const struct TagItem i2crtc_philipsA0[] = {
 { I2CRTC_VENDOR, (ULONG)philips_name },
 { I2CRTC_VENDORID, I2C_VID_PHILIPS },
 { I2C_RTC, 1 }, /* 1 clock */
 { I2C_RDID, 0xa1 },
 { I2C_WRID, 0xa0 },
 { I2CRTC_READCALL, (ULONG)i2c_RTCReadPCF8583 },
 { I2CRTC_WRITECALL, (ULONG)i2c_RTCWritePCF8583 },
 { TAG_DONE, 0 }
};

/* Ricoh at 64/65 */
const struct TagItem i2crtc_ricoh64[] = {
 { I2CRTC_VENDOR, (ULONG)ricoh_name },
 { I2CRTC_VENDORID, I2C_VID_RICOH },
 { I2C_RTC, 1 }, /* 1 clock */
 { I2C_RDID, 0x65 },
 { I2C_WRID, 0x64 },
 { I2CRTC_READCALL, (ULONG)i2c_RTCReadR2025 },
 { I2CRTC_WRITECALL, (ULONG)i2c_RTCWriteR2025 },
 { TAG_DONE, 0 }
};


/*-----------------------------------------------------------------------*/
/* individual chips                                                      */
/*-----------------------------------------------------------------------*/


/* DS1307 */
const struct TagItem i2crtc_ds1307[] = {
 { I2CRTC_DEVICE, (ULONG)ds1307_name },
 { I2CRTC_DEVICEID, I2CRTC_DID_1307},
 { TAG_MORE, (ULONG)i2crtc_dallasD0 }
};

/* DS1629 */
const struct TagItem i2crtc_ds1629[] = {
 { I2CRTC_DEVICE, (ULONG)ds1629_name },
 { I2CRTC_DEVICEID, I2CRTC_DID_1629},
 { TAG_MORE, (ULONG)i2crtc_dallas9E }
};

/* DS3231 */
const struct TagItem i2crtc_ds3231[] = {
 { I2CRTC_DEVICE, (ULONG)ds3231_name },
 { I2CRTC_DEVICEID, I2CRTC_DID_3231 },
 { I2C_TEMP, 1 }, /* this one has also a temp sensor */
 { TAG_MORE, (ULONG)i2crtc_dallasD0 }
};

/* PCF8583 0xA0/0xA1 */
const struct TagItem i2crtc_pcf8583[] = {
 { I2CRTC_DEVICE, (ULONG)pcf8583_name },
 { I2CRTC_DEVICEID, I2CRTC_DID_8583 },
 { TAG_MORE, (ULONG)i2crtc_philipsA0 }
};

/* R2025 0x64/0x65 */
const struct TagItem i2crtc_r2025[] = {
 { I2CRTC_DEVICE, (ULONG)r2025_name },
 { I2CRTC_DEVICEID, I2CRTC_DID_2025 },
 { TAG_MORE, (ULONG)i2crtc_ricoh64 }
};


/*-----------------------------------------------------------------------*/
/* supported chips list                                                  */
/*-----------------------------------------------------------------------*/
/* TODO: solve in another way. */
const struct TagItem i2crtc_chips[] = {
 {I2C_DEVENTRY, (ULONG)i2crtc_r2025     },
 {I2C_DEVENTRY, (ULONG)i2crtc_ds1629    },
 {I2C_DEVENTRY, (ULONG)i2crtc_pcf8583   },
 {I2C_DEVENTRY, (ULONG)i2crtc_philipsA0 },
 {I2C_DEVENTRY, (ULONG)i2crtc_ds1307    },
 {I2C_DEVENTRY, (ULONG)i2crtc_ds3231    },
 {TAG_DONE, 0 }
};






/*
  Get list of supported I2C RTC chips
*/
ASM SAVEDS struct TagItem *i2c_RTCChips( void )
{
	return (struct TagItem *)i2crtc_chips;
}

ASM SAVEDS struct i2c_rtcbase *i2c_RTCOpen( void )
{
	struct i2c_rtcbase *i2c_rtcbase;

	SysBase = *((struct ExecBase **)4UL);

	i2c_rtcbase = (struct i2c_rtcbase*)AllocMem( sizeof( struct i2c_rtcbase ), MEMF_PUBLIC|MEMF_CLEAR );
	if( !i2c_rtcbase )
		return (0);

	i2c_rtcbase->i2clib = OpenLibrary( "i2c.library", 0 );
	i2c_rtcbase->utilitylib =  OpenLibrary( "utility.library", 37 );

	if( (OpenDevice(TIMERNAME, UNIT_VBLANK, &i2c_rtcbase->timer.tr_node, 0L) != 0) || 
	    (!i2c_rtcbase->i2clib) ||
	    (!i2c_rtcbase->utilitylib)
	  )
	{
		i2c_RTCClose( i2c_rtcbase );
		return (0);
	}

	return i2c_rtcbase;
}

ASM SAVEDS LONG i2c_RTCClose( ASMR(a6) struct i2c_rtcbase *i2c_rtcbase   ASMREG(a6) )
{
	if( !i2c_rtcbase )
		return I2C_ERR_BADARG;

	if( i2c_rtcbase->utilitylib)
		CloseLibrary( i2c_rtcbase->utilitylib );

	if( i2c_rtcbase->i2clib )
		CloseLibrary( i2c_rtcbase->i2clib );

	if( i2c_rtcbase->timer.tr_node.io_Device )
		CloseDevice( &i2c_rtcbase->timer.tr_node );

	FreeMem( i2c_rtcbase, sizeof( struct i2c_rtcbase ) );

	return I2C_OK;
}

static struct TagItem *i2c_RTCFindChip(struct i2c_rtcbase *i2c_rtcbase, ULONG devid )
{
	struct TagItem *devtg,*tg,*tlist;

	tlist = (struct TagItem *)i2crtc_chips;
	devtg = (0);
	while( (tg = NextTagItem(&tlist)) ) 
	{
		/* go through device taglist */
		if( tg->ti_Tag == I2C_DEVENTRY )
		{
			/* in each device, search for DEVICEID tag and verify ID */
			devtg = (struct TagItem*)tg->ti_Data;
			if( (tg = FindTagItem(I2CRTC_DEVICEID, devtg ) ))
			{
				if( tg->ti_Data == devid )
					break;
			}
		}
		devtg = (0);
	}

	return devtg;
}

/*
 ReadRTC and WriteRTC apply to RTC chips only: so just pass ClockData
 struct.

 The devid argument can be obtained by parsing the result of i2c_RTCChips().

 The returned data is stored in the ClockData structure only, if you actually
 want to set the system time by it, call i2c_SetSystemTime()

*/
ASM SAVEDS LONG i2c_RTCRead( ASMR(a6) struct i2c_rtcbase *i2c_rtcbase ASMREG(a6),
                      ASMR(d0)  ULONG devid ASMREG(d0), 
                      ASMR(a0) struct ClockData *clockdat ASMREG(a0) 
                    )
{
	struct TagItem *devtg;
	RTCRWCALL(*func);

	devtg = i2c_RTCFindChip( i2c_rtcbase, devid );
	if( !devtg )
		return I2C_ERR_BADID;

	func = (RTCRWCALL(*))GetTagData(I2CRTC_READCALL, (ULONG)i2c_RTCDummy, devtg);

	return (*func)(i2c_rtcbase,clockdat);
}

/*

*/
ASM SAVEDS LONG i2c_RTCWrite( ASMR(a6) struct i2c_rtcbase *i2c_rtcbase ASMREG(a6),
                       ASMR(d0) ULONG devid ASMREG(d0), 
                       ASMR(a0) struct ClockData *clockdat ASMREG(a0) 
		     )
{
	struct TagItem *devtg;
	RTCRWCALL(*func);
	
	devtg = i2c_RTCFindChip( i2c_rtcbase, devid );
	if( !devtg )
		return I2C_ERR_BADID;

	func = (RTCRWCALL(*))GetTagData(I2CRTC_WRITECALL, (ULONG)i2c_RTCDummy, devtg);

	return (*func)(i2c_rtcbase,clockdat);
}



ULONG i2c_RTCChipIDbyName(struct i2c_rtcbase *i2c_rtcbase, char *name )
{
	struct TagItem *devtg,*tg,*tlist;
	ULONG devid = 0;

	tlist = (struct TagItem *)i2crtc_chips;
	while( (tg = NextTagItem(&tlist)) ) 
	{
		/* go through device taglist */
		if( tg->ti_Tag == I2C_DEVENTRY )
		{
			/* in each device, search for DEVICEID tag and verify ID */
			devtg = (struct TagItem*)tg->ti_Data;
			if( (tg = FindTagItem(I2CRTC_DEVICE, devtg ) ))
			{
				if( !Stricmp( (char*)tg->ti_Data, name ) )
				{
					if( (tg = FindTagItem(I2CRTC_DEVICEID, devtg ) ))
					{
						devid = tg->ti_Data;
						break;
					}
				}
			}
		}
	}

	return devid;
}


ASM SAVEDS LONG i2c_RTCSetSystemTime( ASMR(a6) struct i2c_rtcbase *i2c_rtcbase ASMREG(a6),
                               ASMR(a0) struct ClockData *clockdat ASMREG(a0)
                             )
{
	if( (!i2c_rtcbase) || (!clockdat) )
		return I2C_ERR_BADARG;

	i2c_rtcbase->timer.tr_time.tv_secs = CheckDate(clockdat);
	i2c_rtcbase->timer.tr_node.io_Command = TR_SETSYSTIME;

	/* Printf("Time in secs %ld\n",i2c_rtcbase->timer.tr_time.tv_secs); */
 	
	if( DoIO( &i2c_rtcbase->timer.tr_node ) )
	{
		return I2C_ERR_BADARG;
	}

	return I2C_OK;
}

ASM SAVEDS LONG i2c_RTCGetSystemTime( ASMR(a6) struct i2c_rtcbase *i2c_rtcbase ASMREG(a6),
                               ASMR(a0) struct ClockData *clockdat ASMREG(a0)
                             )
{
	struct timeval tv;

	if( (!i2c_rtcbase) || (!clockdat) )
		return I2C_ERR_BADARG;

	GetSysTime( &tv );
	Amiga2Date(tv.tv_secs, clockdat );

	return I2C_OK;
}


LONG i2c_RTCDummy( struct i2c_rtcbase *i2c_rtcbase,struct ClockData *clockdat)
{
	return I2C_ERR_NOTFOUND;
}

/* 8 bit binary (0-99) to 8 Bit BCD */
UBYTE BINBCD( USHORT in )
{
 USHORT ret;
 USHORT i;

 ret = in<<3;
 for( i=0 ; i<5 ; i++ )
 {
	if( (ret&0xf00) > 0x400 )
		ret += 0x300;
	ret<<=1;
 }

 return (UBYTE)(ret>>8);
}

UBYTE BCDBIN( UBYTE in )
{
	UBYTE res = in>>4;

	res += (res<<3)+res;
	res += in & 0xf;

	return res;
}

//#define BINBCD(a) ( (((a)%10)<<4) + (a/10) )

/* min. 7 bytes space in "buf" */
static void i2c_clockdat_to_secminhourwdaymdaymonthyear( UBYTE *buf, struct ClockData *clockdat)
{
	buf[0] = BINBCD( clockdat->sec  );
	buf[1] = BINBCD( clockdat->min  );
	buf[2] = BINBCD( clockdat->hour );
	buf[3] = BINBCD( clockdat->wday );
	buf[4] = BINBCD( clockdat->mday );
	buf[5] = BINBCD( clockdat->month);
	buf[6] = BINBCD( clockdat->year-1976 );
}

/* min. 7 bytes in buf: sec,min,hour,wday,mday,month,year-1976 */
static void i2c_secminhourwdaymdaymonthyear_to_clockdat( UBYTE *buf, struct ClockData *clockdat, LONG flags)
{
	clockdat->sec   = BCDBIN( buf[0] & 0x7f );
	clockdat->min   = BCDBIN( buf[1] & 0x7f );
	clockdat->hour  = BCDBIN( buf[2] & 0x3f );
	clockdat->wday  = BCDBIN( buf[3] & 0x0f );
	clockdat->mday  = BCDBIN( buf[4] & 0x3f );
	clockdat->month = BCDBIN( buf[5] & 0x1f );
	if( flags & 0x40 )
	{
		clockdat->year  = 1976+(USHORT)BCDBIN( buf[6] );
		if( clockdat->year < 1978 )
		   	clockdat->year = 1978;
	}
}

/* ------------------------------------------------------------------------ */
/* RTC Read subfunction for DS1307/DS3231                                   */
/* ------------------------------------------------------------------------ */
LONG i2c_RTCReadDS1307( struct i2c_rtcbase *i2c_rtcbase,struct ClockData *clockdat)
{
 UBYTE rwbuf[8];
 LONG  res; 

 rwbuf[0] = 0x00;      /* start address in clock space */
 res      = SendI2C( 0xD0, 1, rwbuf );

 if( res & 0xf ) /* start address 0 accepted? */
 {
	res = ReceiveI2C( 0xD0, 7, rwbuf );
	if( res & 0xf )
	{
	 i2c_secminhourwdaymdaymonthyear_to_clockdat( rwbuf, clockdat, 0x7f );
	}
 }

 if( !(res & 0xf ) )
 	return I2C_ERR_NOTFOUND;
 else
	return I2C_OK;

}

/* ------------------------------------------------------------------------ */
/* RTC Write subfunction for DS1307/DS3231                                  */
/* ------------------------------------------------------------------------ */
LONG i2c_RTCWriteDS1307( struct i2c_rtcbase *i2c_rtcbase,struct ClockData *clockdat)
{
 UBYTE rwbuf[8];
 LONG res = 0x10;       /* def: error */

 rwbuf[0]  = 0x00;	/* start address in clock space */
 i2c_clockdat_to_secminhourwdaymdaymonthyear( &rwbuf[1], clockdat);
 rwbuf[1] |= 0x80;      /* stop clock flag */ 

 if( (0xf & SendI2C( 0xD0, 2, rwbuf ) ) ) /* clock stopped successfully ? */
 {
   res = SendI2C( 0xD0, 8, rwbuf ); /* try to set clock */
 }

 rwbuf[1] &= 0x7f;
 SendI2C( 0xD0, 2, rwbuf ); /* even if an error occured, try to make clock run */

 if( !(res & 0xf ) )
 	return I2C_ERR_NOTFOUND;
 else
	return I2C_OK;
}


/* ------------------------------------------------------------------------ */
/* RTC Read subfunction for DS1609                                          */
/* ------------------------------------------------------------------------ */
LONG i2c_RTCReadDS1609( struct i2c_rtcbase *i2c_rtcbase,struct ClockData *clockdat)
{
 UBYTE rwbuf[8];
 LONG  res; 

 rwbuf[0] = 0xC0;      /* access code to clock */
 rwbuf[1] = 0x00;      /* start address in clock space */
 res      = SendI2C( 0x9E, 2, rwbuf );

 if( res & 0xf ) /* start address 0 accepted? */
 {
	res = ReceiveI2C( 0x9E, 7, rwbuf );
	if( res & 0xf )
	{
	 i2c_secminhourwdaymdaymonthyear_to_clockdat( rwbuf, clockdat, 0x7f );
	}
 }

 if( !(res & 0xf ) )
 	return I2C_ERR_NOTFOUND;
 else
	return I2C_OK;
}


/* ------------------------------------------------------------------------ */
/* RTC Write subfunction for DS1609                                         */
/* ------------------------------------------------------------------------ */
LONG i2c_RTCWriteDS1609( struct i2c_rtcbase *i2c_rtcbase,struct ClockData *clockdat)
{
 UBYTE rwbuf[10];
 LONG res = 0x10;       /* def: error */

 rwbuf[0]  = 0xC0;      /* access code to clock */
 rwbuf[1]  = 0x00;	/* start address in clock space */
 i2c_clockdat_to_secminhourwdaymdaymonthyear( &rwbuf[2], clockdat);
 rwbuf[2] |= 0x80;      /* stop clock flag */ 

 if( (0xf & SendI2C( 0x9E, 3, rwbuf ) ) ) /* clock stopped successfully ? */
 {
   res = SendI2C( 0x9E, 9, rwbuf ); /* try to set clock */
 }

 rwbuf[2] &= 0x7f;
 SendI2C( 0x9E, 3, rwbuf ); /* even if an error occured, try to make clock run */

 if( !(res & 0xf ) )
 	return I2C_ERR_NOTFOUND;
 else
	return I2C_OK;

}

/* ------------------------------------------------------------------------ */
/* RTC Read subfunction for PCF8583                                         */
/* ------------------------------------------------------------------------ */
#define I2C_PHILIPSA0 0xa0
#define PHIL_YEAR_EXTRA	0x10 /* chip doesn't have proper year counter, use extra regs */
LONG i2c_RTCReadPCF8583( struct i2c_rtcbase *i2c_rtcbase,struct ClockData *clockdat)
{
 UBYTE rwbuf[10];
 LONG  res; 
 LONG  year;
 LONG  rtc_saved_year,rtc_year;

 rwbuf[0] = 0x00;      /* control register address in clock space */
 rwbuf[1] = 0x00;      /* clear hold in control register */
 res      = SendI2C( I2C_PHILIPSA0, 2, rwbuf );
 if( !(res & 0xf) ) /* device accepted? */
 	goto err;

 /* main part */

	/* a) get year information from extra location and verify content */
	/*    the PCF8583 stores a 4 year cycle only                      */
	rwbuf[0] = PHIL_YEAR_EXTRA;
	SendI2C( I2C_PHILIPSA0, 1, rwbuf );

	res = ReceiveI2C( I2C_PHILIPSA0, 1, rwbuf );
	if( !(res & 0xf) )
		goto err;

	rtc_saved_year = (rwbuf[0] & 0xC0) >> 6;
	year = rwbuf[0] & 0x3f;

	if( year > 33 )
		year = 33;
	if( (!year) && (rtc_saved_year < 2) )
		rtc_saved_year = 2;
	
	/* b) get time/date */
	rwbuf[0] = 0x00;      /* control register address in clock space */
	rwbuf[1] = 0x40;      /* set hold in control register */
	res      = SendI2C( I2C_PHILIPSA0, 2, rwbuf );
	if( !(res & 0xf) ) /* device accepted? */
 		goto err;

	res = ReceiveI2C( I2C_PHILIPSA0, 6, &rwbuf[2] );
	if( !(res & 0xf) ) /* device accepted? */
 		goto err;

	rwbuf[0] = 0x00;
	rwbuf[1] = 0x00;	/* clear hold */
	res = SendI2C( I2C_PHILIPSA0, 2, rwbuf );
	if( !(res & 0xf) ) /* device accepted? */
		goto err;

	/* rwbuf[2...7] = clock contents
	   rwbuf[2] = microsec
	   rwbuf[3] = sec
	   rwbuf[4] = min
	   rwbuf[5] = hour
	   rwbuf[6] = day of month (6 bit), year (2 Bit)<<6
	   rwbuf[7] = weekday (3 Bit)<<5, month (5 Bit)
	*/
	rwbuf[8] = rwbuf[7]; /* copy month, (also weekday<<5) */
	rwbuf[7] = rwbuf[6]; /* copy day of month (6 bit), also (year&3)<<6 */
	rwbuf[6] = (rwbuf[8]>>5);
	rtc_year = (rwbuf[7]>>6)&3;
	/* reordered for the function with the ridiculous name :-/
	   rwbuf[6] = weekday
	   rwbuf[7] = day of month (6 bit), year (2 Bit)<<6
	   rwbuf[8] = month, weekday<<5
	*/
	i2c_secminhourwdaymdaymonthyear_to_clockdat( &rwbuf[3], clockdat, 0x3f ); /* keep year out */

	/* year considerations */
	if( rtc_saved_year != rtc_year )
	{
		if( rtc_year > rtc_saved_year )
			year++;

		rwbuf[0] = PHIL_YEAR_EXTRA;
		rwbuf[1] = (rtc_year << 6) | year;
		SendI2C( I2C_PHILIPSA0, 2, rwbuf );
	}
	clockdat->year = 1976 + (year<<2) + rtc_year;


err:
	if( !(res & 0xf ) )
	 	return I2C_ERR_NOTFOUND;
	else
		return I2C_OK;
}

/* ------------------------------------------------------------------------ */
/* RTC Write subfunction for PCF8583                                        */
/* ------------------------------------------------------------------------ */
LONG i2c_RTCWritePCF8583( struct i2c_rtcbase *i2c_rtcbase,struct ClockData *clockdat)
{
	UBYTE rwbuf[12],md,mon;
	LONG  res,year; 


	rwbuf[0] = 0x00;     /* control register */
	rwbuf[1] = 0x80;     /* stop clock       */
	rwbuf[2] = 0x00;     /* microseconds (irrelevant) */
	i2c_clockdat_to_secminhourwdaymdaymonthyear( &rwbuf[3], clockdat);

	year = clockdat->year - 1976;

	/* re-shuffle buffer for PCF8583 */
	md = rwbuf[7] | ((year&3)<<6); /* rwbuf[7] = BINBCD(clockdat->mday) */
	mon= rwbuf[8] | (rwbuf[6]<<5); /* rwbuf[8] = BINBCD(clockdat->month) */

	rwbuf[6] = md;  /* day of month (6 bit), year (2 Bit)<<6 */
	rwbuf[7] = mon; /* weekday (3 Bit)<<5, month (5 Bit) */

	res = SendI2C( I2C_PHILIPSA0, 8, rwbuf );
	if( !(res & 0xf) ) /* device accepted? */
	       goto err;


	rwbuf[1] = 0x00; /* clear clock stop */
	res = SendI2C( I2C_PHILIPSA0, 2, rwbuf );
	if( !(res & 0xf) ) /* device accepted? */
	       goto err;   /* this is bad but we can't help it */


	rwbuf[0] = PHIL_YEAR_EXTRA;
	rwbuf[1] = (year>>2) | ((year&3)<<6); /* year = offset from 1976 */

	res = SendI2C( I2C_PHILIPSA0, 2, rwbuf );

err:
	if( !(res & 0xf ) )
 		return I2C_ERR_NOTFOUND;
	else
		return I2C_OK;
}





/* ------------------------------------------------------------------------ */
/* RTC Read subfunction for R2025                                           */
/* ------------------------------------------------------------------------ */
#define I2C_RICOH64 0x64
LONG i2c_RTCReadR2025( struct i2c_rtcbase *i2c_rtcbase,struct ClockData *clockdat)
{
 UBYTE rwbuf[16];
 LONG  res; 

   res = ReceiveI2C( I2C_RICOH64, 16, rwbuf );
   if( !(res & 0xf) ) /* device accepted? */
 	goto err;

   i2c_secminhourwdaymdaymonthyear_to_clockdat( &rwbuf[1], clockdat, 0x7f );

err:
	if( !(res & 0xf ) )
	 	return I2C_ERR_NOTFOUND;
	else
		return I2C_OK;
}


/* ------------------------------------------------------------------------ */
/* RTC Write subfunction for R2025                                          */
/* ------------------------------------------------------------------------ */
LONG i2c_RTCWriteR2025( struct i2c_rtcbase *i2c_rtcbase,struct ClockData *clockdat)
{
 UBYTE rwbuf[16];
 LONG  res;


	rwbuf[0] = 0xE0;     /* control register 1 */
	rwbuf[1] = 0x20;     /* control register 1 config */
	rwbuf[2] = 0xA0;     /* control register 2 config */
	i2c_clockdat_to_secminhourwdaymdaymonthyear( &rwbuf[3], clockdat);
	rwbuf[10] = 0x7F;    /* oscillator adjustment 3ppm */
	/* TODO: century bit support -> pointless since Amiga time expires shortly, anyway */

	res = SendI2C( I2C_RICOH64, 11, rwbuf );

	if( !(res & 0xf ) )
	 	return I2C_ERR_NOTFOUND;
	else
		return I2C_OK;
}


