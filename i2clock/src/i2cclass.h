/*
  file:   i2cclass.h

  proposal for I2C class system

  author: Henryk Richter <henryk.richter@gmx.net>

*/
#ifndef _INC_I2CCLASS_H
#define _INC_I2CCLASS_H

#ifndef UTILITY_TAGITEM_H
#include <utility/tagitem.h>
#endif

/* open stuff 
   - devices that fall into several classes like RTC with Temp Sensor
   -> Q: Do we provide a generic interface for specific functions
         like "get all temps" instead of concentrating on devices
	 to be polled?

   - I2C addresses apply to different devices.
   -> Q: is it better to "guess" the chip type or rather offer
         the user a choice of possible chips and save the result
	 in a set of ENV Variables?

*/


/* ------------------------------------------------------------------------ */
/* I2C Device capabilies                                                    */
#define I2C_BASE TAG_USER+0x012c0000

#define I2C_CUSTOM   (I2C_BASE+0x00) /* unspecified chip */
#define I2C_RDID     (I2C_BASE+0x01) /* read ID, i.e. chip supports read    */
#define I2C_WRID     (I2C_BASE+0x02) /* write ID, i.e. chip supports write  */
#define I2C_DEVENTRY (I2C_BASE+0x02) /* this points to a taglist            */

/* sensors */
#define I2C_VOLTAGE  (I2C_BASE+0x10)
#define I2C_CURRENT  (I2C_BASE+0x11)
#define I2C_TEMP     (I2C_BASE+0x12)
#define I2C_FAN      (I2C_BASE+0x13)
#define I2C_PRESSURE (I2C_BASE+0x14)
#define I2C_RTC      (I2C_BASE+0x15)

/* ------------------------------------------------------------------------ */

/* vendors in here */
#define I2C_VIDBASE I2C_BASE+0x100
#define I2C_VID_UNKNOWN (I2C_VIDBASE+0)
#define I2C_VID_PHILIPS (I2C_VIDBASE+1)
#define I2C_VID_DALLAS  (I2C_VIDBASE+2)
#define I2C_VID_RICOH   (I2C_VIDBASE+3)
#define I2C_VID_BOSCH   (I2C_VIDBASE+4)

/* ------------------------------------------------------------------------ */
/* error codes                                                              */
/* ------------------------------------------------------------------------ */
#define I2C_OK            1
#define I2C_ERR_NOTFOUND -1 /* chip does not respond */
#define I2C_ERR_BADID    -2 /* chip ID unknown */
#define I2C_ERR_BADARG   -3 /* */

#endif /* _INC_I2CCLASS_H */
