/*
  file:   i2cclass_rtc.h

  proposal for I2C class by the example of RTC module support

  author: Henryk Richter <henryk.richter@gmx.net>

*/
#ifndef _INC_I2CCLASS_RTC_H
#define _INC_I2CCLASS_RTC_H

#ifndef _INC_I2CCLASS_H
#include "i2cclass.h" /* will load utility/tagitem.h */
#endif

#include <utility/date.h>
#include <devices/timer.h>

#include "compiler.h"

/* ------------------------------------------------------------------ */


/* ------------------------------------------------------------------ */
/* "I2C RTC base" (I2C_BASE+$c000), lowest would be 0x200             */
#define I2CRTC_BASE I2C_BASE+0xc000

/* I2C ID for reading/writing, if applicable (for I2C scan)           */
#define I2CRTC_RDID      (I2CRTC_BASE+1)
#define I2CRTC_WRID      (I2CRTC_BASE+2)

/* I2C Vendor string */
#define I2CRTC_VENDOR    (I2CRTC_BASE+3)
/* INTERNAL Vendor ID, unique TAG specific to class */
#define I2CRTC_VENDORID  (I2CRTC_BASE+4)

/* I2C Device String */
#define I2CRTC_DEVICE    (I2CRTC_BASE+5)
/* INTERNAL Device ID, unique TAG specific to class */
#define I2CRTC_DEVICEID  (I2CRTC_BASE+6)
/* INTERNAL function pointers */
#define I2CRTC_READCALL  (I2CRTC_BASE+7)
#define I2CRTC_WRITECALL (I2CRTC_BASE+8)

/* ------------------------------------------------------------------ */

#define I2CRTC_DID_1307 0xd1307
#define I2CRTC_DID_1629 0xd1629
#define I2CRTC_DID_3231 0xd3231

#define I2CRTC_DID_8583 0xb8583
#define I2CRTC_DID_2025 0xc2025

/* ------------------------------------------------------------------ */
/* structures                                                         */
/* ------------------------------------------------------------------ */
struct i2c_rtcbase {
	struct Library *i2clib;
	struct Library *utilitylib;
	struct timerequest timer;
};

/* ------------------------------------------------------------------ */
/* Functions                                                          */
/* ------------------------------------------------------------------ */
/*
   concept: classic open() / do_something() / close()

   weak points:
    - the DeviceID (ULONG in RTCRead/RTCWrite) is somewhat bad
      right now. To get it requires prior knowledge, basically
      parsing the TagList from i2c_RTCChips() for the assigned
      id for a specific device type. 
    - This DeviceID concept might be useful for application scenarios
      where the user pre-selects chips by a configuration program
      such that respective DeviceIDs are stored somewhere in ENV:
    -> qualifies as FIXME
*/

/* main API */
ASM SAVEDS struct TagItem *i2c_RTCChips( void );

ASM SAVEDS struct i2c_rtcbase *i2c_RTCOpen( void );

ASM SAVEDS LONG i2c_RTCClose( ASMR(a6) struct i2c_rtcbase * ASMREG(a6) );

ASM SAVEDS LONG i2c_RTCRead(  ASMR(a6) struct i2c_rtcbase * ASMREG(a6),
                       ASMR(d0)  ULONG               ASMREG(d0), 
                       ASMR(a0) struct ClockData *   ASMREG(a0) 
		    );
ASM SAVEDS LONG i2c_RTCWrite( ASMR(a6) struct i2c_rtcbase * ASMREG(a6),
                       ASMR(d0) ULONG                ASMREG(d0), 
                       ASMR(a0) struct ClockData *   ASMREG(a0) 
		     );

/* utility functions */

/* convert chip name (like "DS1307" to internal DeviceID) */
ULONG i2c_RTCChipIDbyName(struct i2c_rtcbase *, char * );

/* just take stuct ClockData and set/get system time (by timer.device) */
ASM SAVEDS LONG i2c_RTCSetSystemTime( ASMR(a6) struct i2c_rtcbase * ASMREG(a6),
                               ASMR(a0) struct ClockData *   ASMREG(a0) );
ASM SAVEDS LONG i2c_RTCGetSystemTime( ASMR(a6) struct i2c_rtcbase * ASMREG(a6),
                               ASMR(a0) struct ClockData *   ASMREG(a0) );



/* internal functions (deliberately not using exactly 
   specified calling conventions *evil grin*)         */
#define RTCRWCALL(_a_) LONG (_a_)(struct i2c_rtcbase *,struct ClockData *)
LONG i2c_RTCDummy( struct i2c_rtcbase *,struct ClockData *);

LONG i2c_RTCWriteDS1307( struct i2c_rtcbase *,struct ClockData *);
LONG i2c_RTCReadDS1307( struct i2c_rtcbase *,struct ClockData *);

LONG i2c_RTCWriteDS1609( struct i2c_rtcbase *,struct ClockData *);
LONG i2c_RTCReadDS1609( struct i2c_rtcbase *,struct ClockData *);

LONG i2c_RTCWritePCF8583( struct i2c_rtcbase *,struct ClockData *);
LONG i2c_RTCReadPCF8583( struct i2c_rtcbase *,struct ClockData *);

LONG i2c_RTCWriteR2025( struct i2c_rtcbase *,struct ClockData *);
LONG i2c_RTCReadR2025( struct i2c_rtcbase *,struct ClockData *);






#endif /* _INC_I2CCLASS_RTC_H */
