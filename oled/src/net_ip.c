#include<stdio.h>	//printf
#include<string.h>	//memset
#include<errno.h>	//errno
#include<sys/socket.h>	//socket
#include<netinet/in.h> //sockaddr_in
#include<arpa/inet.h>	//getsockname
#include<unistd.h>	//close

struct Library *SocketBase;

int errno;
int h_errno;


ULONG get_ipv4(void)
{
	struct TagItem tags[5];
	int status;
	const char* google_dns_server = "8.8.8.8";
	int dns_port = 53;
	struct sockaddr_in serv;
	int sock,err;
	struct sockaddr_in name;
	socklen_t namelen;
	char buffer[100];
	const char*p;
	ULONG ip;

	SocketBase = NULL;
	sock = -1;
	ip = -1;
  do{
	if(!FindName(&SysBase->LibList, "bsdsocket.library"))
		break;

	SocketBase = OpenLibrary("bsdsocket.library",3);i

	tags[0].ti_Tag	= SBTM_SETVAL(SBTC_ERRNOLONGPTR);
	tags[0].ti_Data	= (ULONG)&errno;

	tags[1].ti_Tag = SBTM_SETVAL(SBTC_BREAKMASK);
	tags[1].ti_Data	= 0; /* TODO: break signal mask */

	tags[2].ti_Tag	= SBTM_SETVAL(SBTC_LOGTAGPTR);
	tags[2].ti_Data	= (ULONG)__program_name;

	tags[3].ti_Tag	= SBTM_SETVAL(SBTC_HERRNOLONGPTR);
	tags[3].ti_Data	= (ULONG)&h_errno;

	tags[4].ti_Tag = TAG_END;

	status = SocketBaseTagList(tags);


	sock = socket ( AF_INET, SOCK_DGRAM, 0);
 	// Socket could not be created
	if(sock < 0)
		break;
    
	memset( &serv, 0, sizeof(serv) );
	serv.sin_family = AF_INET;
	serv.sin_addr.s_addr = inet_addr( google_dns_server );
	serv.sin_port = htons( dns_port );

	err = connect( sock , (const struct sockaddr*) &serv , sizeof(serv) );
    
 	namelen = sizeof(name);
	err = getsockname(sock, (struct sockaddr*) &name, &namelen);
    
    	ip = name.sin_addr;

	p = inet_ntop(AF_INET, &name.sin_addr, buffer, 100);
	if(p != NULL)
	{
		printf("Local ip is : %s \n" , buffer);
	}
	else
	{
		//Some error
		printf ("Error number : %d . Error message : %s \n" , errno , strerror(errno));
	}
	
	close(sock);

  } while(0);

	if(SocketBase)
		CloseLibrary(SocketBase);

 return ip;
}
