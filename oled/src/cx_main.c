/*
  cx_main.c

  (C)2020 Henryk Richter <henryk.richter@gmx.net>

  purpose:
   This is the commodity code. It sets up a broker
   alongside an intuition filter and checks for wheel
   movement periodically.


*/
#include <exec/types.h>
#include <exec/execbase.h>
#include <exec/memory.h>
#include <exec/ports.h>
#include <string.h>
#include <intuition/intuitionbase.h>
#include <libraries/commodities.h>
#include <devices/input.h>

#define __NOLIBBASE__
#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/utility.h>
#include <proto/commodities.h>
#include <proto/i2csensors.h>
#include <libraries/i2csensors.h>

#include "cx_main.h"
#include "ssd1306.h"

const STRPTR cx_Name = (STRPTR)"SSD1306Sensors";
STRPTR cx_Desc = (STRPTR)"System stats on OLED," \
		 "H. Richter (C) 2020";

struct cx_Custom {
	struct MsgPort  *eventport;
	struct IOStdReq *eventreq;
	struct InputEvent evt;
};
#if 0
#define JOY1DAT 0xDFF00C
/* this is from newmouse.h by Alessandro Zumo */
#define IECLASS_NEWMOUSE        (0x16)  /* IECLASS_MAX + 1 as of V40 */ 
#define NM_WHEEL_UP             (0x7A)
#define NM_WHEEL_DOWN           (0x7B)
#define NM_WHEEL_LEFT           (0x7C)
#define NM_WHEEL_RIGHT          (0x7D)
#define NM_BUTTON_FOURTH        (0x7E)
#endif

/* maximum length of one row */
#define MAXLEN 16
/* I2C Address of slave */
#define I2C_DEF 0x80

/* interval in minutes (5 min here) */
#define DEF_INTERVAL 5

#if 0
/* enable this to activate the intuition ticks */
#define GET_TICKS_FILTER
#endif

/* Quit when started a second time ? */
#define UNIQUE_QUIT


/*		"This software translates signals from the joystick port\n" \
		 "into auxiliary button clicks and wheel movement.\n" \ */

struct IntuitionBase *IntuitionBase;
struct Library       *CxBase;
struct GfxBase       *GfxBase;
struct Library       *UtilityBase;
struct Library       *I2CSensorsBase;
struct Library       *TimerBase;

struct ssd1306disp *disp;

LONG   openLibs( void );
LONG   closeLibs( void );
CxObj *cx_Setup( struct MsgPort *cx_port, struct configvars *conf, struct cx_Custom *cust );
LONG   cx_Dispose( CxObj *cx_Broker, struct cx_Custom *cust ); 
LONG   cx_Handle_Msg( CxObj *cx_Broker, CxMsg *msg );

LONG SendDataToI2C( struct configvars *conf );

#ifdef GET_TICKS_FILTER
/* filter function that pings us by signal with every intuition tick */
LONG cx_AddEventFilter( CxObj *cx_Broker, struct Task *self, ULONG signal );
LONG cx_Handle_Signal( struct cx_Custom *cust );

void oneEvent( struct cx_Custom *cust, ULONG code, ULONG class );
void sendEvents( struct cx_Custom *cust, ULONG code, ULONG count );
#endif


LONG cx_main( struct configvars *conf )
{
  struct MsgPort *cx_Port = NULL;
#ifdef GET_TICKS_FILTER
  ULONG  cx_Signal = -1;
#endif
  CxObj *cx_Broker = NULL;
  ULONG keeprunning;
  struct cx_Custom cust;
  ULONG interval;

  struct MsgPort *TimerPort = NULL;
  struct timerequest *timerio = NULL;

  cust.eventport = 0;
  cust.eventreq  = 0;
#if 0
  cust.lastXY = *( (UWORD*)JOY1DAT ); /* last content of JOY1DAT */
  cust.swapX = (conf->reverse) ? 1 : 0;
  cust.swapY = (conf->reverse) ? 1 : 0;
  if( conf->reversex )
	  cust.swapX ^= 1;
  if( conf->reversey )
	  cust.swapY ^= 1;
#endif

  do
  {
	disp = NULL;

	if( openLibs() )
		break;
#ifdef GET_TICKS_FILTER
	cx_Signal = AllocSignal( -1 );
	if( cx_Signal < 0 )
		break;
#endif
	cx_Port = CreateMsgPort();
	if( !cx_Port )
		break;

	TimerPort = CreateMsgPort();
	if( !TimerPort )
		break;
	timerio = (struct timerequest *)CreateIORequest(TimerPort, sizeof(struct timerequest));
	if( !timerio )
		break;
	if( OpenDevice( (STRPTR)TIMERNAME, UNIT_VBLANK, (struct IORequest *) timerio, 0) != 0 )
		break;
	TimerBase = (struct Library*)timerio->tr_node.io_Device;
	timerio->tr_node.io_Command = TR_ADDREQUEST;
	timerio->tr_time.tv_secs = 1;
	timerio->tr_time.tv_micro = 1;
	SendIO((struct IORequest *) timerio);
	interval = (conf->interval) ? *conf->interval : DEF_INTERVAL;
	interval = (interval<<6) - (interval<<2); /* minutes * 60 = seconds */

	if( !(cx_Broker = cx_Setup( cx_Port, conf, &cust )) )
		break;

#ifdef GET_TICKS_FILTER
	/* extra: we want intuition ticks in this program (by signal) */
	cx_AddEventFilter( cx_Broker, FindTask(NULL) , cx_Signal );
#endif

	disp = ssd1306_init( (conf->i2caddr) ? *conf->i2caddr : 0 );
	if( !disp )
		break;
	ssd1306_clear( disp );

	/* main loop */
	keeprunning = 1;
	do
	{
		struct Message *msg;

		/* SIGBREAKF_CTRL_F with window */
		ULONG signals = (1<<cx_Port->mp_SigBit) | SIGBREAKF_CTRL_C | SIGBREAKF_CTRL_E;
		signals |= (1<<TimerPort->mp_SigBit);
#ifdef GET_TICKS_FILTER
		signals |= (1<<cx_Signal); 
#endif
		signals = Wait( signals );
		
		if( signals & (SIGBREAKF_CTRL_C | SIGBREAKF_CTRL_E) )
			break;

		if( signals & 1<<TimerPort->mp_SigBit)
		{
			WaitIO((struct IORequest *) timerio);
			timerio->tr_node.io_Command = TR_ADDREQUEST;
			timerio->tr_time.tv_secs = interval;
			timerio->tr_time.tv_micro = 1;
			SendIO((struct IORequest *) timerio);

			SendDataToI2C( conf );
		}

#ifdef GET_TICKS_FILTER
		if( signals & (1<<cx_Signal) )
		{
			cx_Handle_Signal(&cust);
		}
#endif

		while( (msg = GetMsg( cx_Port )) )
		{
			/* only returns 0 if a kill event happened */
			if( !cx_Handle_Msg( cx_Broker, (CxMsg*)msg ) )
				keeprunning = 0;
		}
	}
	while( keeprunning );
  }
  while(0);

  if( disp )
	ssd1306_end( disp );


  /* Close Timer */
  if( TimerBase )
  {
	AbortIO((struct IORequest *) timerio);
        CloseDevice((struct IORequest *) timerio);
  }
  if( timerio )
  {
  	DeleteIORequest((struct IORequest *)timerio);
  }
  if( TimerPort )
  {
  	DeleteMsgPort( TimerPort );
  }

  /* Cleanup */
  cx_Dispose( cx_Broker, &cust ); /* checks for NULL */

#ifdef GET_TICKS_FILTER
  if( cx_Signal > 0 )
  	FreeSignal( cx_Signal );
#endif
  if( cx_Port )
  {
  	struct Message *msg;

	while( (msg = GetMsg(cx_Port)))
		ReplyMsg(msg);

  	DeleteMsgPort( cx_Port );
  }
  closeLibs();

  return 0;
}



LONG openLibs( void )
{
	LONG res = 0;

	IntuitionBase = NULL;
	CxBase = NULL;
	GfxBase = NULL;
	UtilityBase = NULL;
	I2CSensorsBase = NULL;

	if( !(IntuitionBase = (struct IntuitionBase*)OpenLibrary( (STRPTR)"intuition.library",37)))
		res = 1;
	if( !(CxBase = OpenLibrary( (STRPTR)"commodities.library",37)))
		res = 1;
	if( !(GfxBase = (struct GfxBase*)OpenLibrary( (STRPTR)"graphics.library",37)))
		res = 1;
	if( !(UtilityBase = OpenLibrary( (STRPTR)"utility.library",37)))
		res = 1;
	if( !res )
	{
	    if( !(I2CSensorsBase = OpenLibrary( (STRPTR)"i2csensors.library",1)))
	    {
	        ULONG iflags = 0;
   			struct EasyStruct libnotfoundES = {
		           sizeof (struct EasyStruct),
	        	   0,
		            (STRPTR)"blah",
			        (STRPTR)"I2CSensors.library not found. Quitting.",
		            (STRPTR)"OK",
		       };
		EasyRequest( NULL, &libnotfoundES, &iflags );
		res = 1;
	    }
	}

	return res;
}

LONG closeLibs( void )
{
	if( I2CSensorsBase )
		CloseLibrary( (struct Library *)I2CSensorsBase );
	if( IntuitionBase )
		CloseLibrary( (struct Library *)IntuitionBase );
	if( CxBase )
		CloseLibrary( CxBase );
	if( GfxBase )
		CloseLibrary( (struct Library *)GfxBase );
	if( UtilityBase )
		CloseLibrary( UtilityBase );

	return 0;
}


LONG   cx_Handle_Msg( CxObj *cx_Broker, CxMsg *msg )
{
	LONG ret = 1;
	ULONG id,type;

	id  = CxMsgID(msg);
	type= CxMsgType(msg);
	ReplyMsg( (struct Message*)msg );

#if 0
	/* no window as of yet */
	if( type == CXM_IEVENT )
	{
		switch( id )
		{
			case MY_POPKEY_ID: 
				/* OPEN WINDOW */
				break;
			default: 
				break;
		}
	}
#endif
	if( type == CXM_COMMAND )
	{
		switch( id )
		{
			case CXCMD_KILL:	ret=0;break;
			case CXCMD_DISABLE:	ActivateCxObj(cx_Broker,0);break;
			case CXCMD_ENABLE:	ActivateCxObj(cx_Broker,1);break;
#ifdef UNIQUE_QUIT
			case CXCMD_UNIQUE:	ret=0;break;
#endif
#if 0			
			/* Open/Close WINDOW */
			case CXCMD_APPEAR:
			case CXCMD_DISAPPEAR:
#endif
			default: break;
		}
	}

	return ret;
}


CxObj *cx_Setup( struct MsgPort *cx_port, struct configvars *conf, struct cx_Custom *cust )
{
	struct NewBroker nb;
	CxObj *cx_Broker;

	nb.nb_Version = NB_VERSION;
	nb.nb_Name    = cx_Name;
	nb.nb_Title   = cx_Name;
	nb.nb_Descr   = cx_Desc;
	nb.nb_Unique  = NBU_NOTIFY|NBU_UNIQUE;
	nb.nb_Flags   = 0; /* COF_SHOW_HIDE */
	nb.nb_Pri     = (conf->pri) ? *(conf->pri) : 0;
	nb.nb_Port    = cx_port;
	nb.nb_ReservedChannel = 0;

	if( (cx_Broker = CxBroker(&nb,NULL)) )
	{
		LONG err;
#if 0
		{
		 /* we don't have a window, so don't need a popup keyboard shortcut */
	         CxObj *filter = CxFilter(MY_POPKEY_STR); /* e.g. "ctrl lalt m" */
		 AttachCxObj(filter,CxSender(cx_port,MY_POPKEY_ID)); /* ID we get on messages */
		 AttachCxObj(filter,CxTranslate(NULL));
	         AttachCxObj(cx_Broker,filter);
		}
#endif

#ifdef GET_TICKS_FILTER
		cust->eventport = CreateMsgPort();
		cust->eventreq  = CreateIORequest( cust->eventport,sizeof(struct IOStdReq));
		err = OpenDevice("input.device",NULL,(struct IORequest *)cust->eventreq,NULL);
#else
		err = 0;
#endif
		if( (CxObjError(cx_Broker) == 0) && (err==0) )
		{
			ActivateCxObj(cx_Broker,1);
		}
		else
		{
			DeleteCxObjAll(cx_Broker);
			if( cust->eventreq )
				DeleteIORequest( cust->eventreq );
			if( cust->eventport )
				DeleteMsgPort( cust->eventport );
			cust->eventport = NULL;
			cust->eventreq = NULL;
			cx_Broker = NULL;
		}
	}
	
	return cx_Broker;
}

LONG cx_Dispose( CxObj *cx_Broker, struct cx_Custom *cust )
{
	if( cx_Broker )
	{
		DeleteCxObjAll( cx_Broker );
	}
#ifdef GET_TICKS_FILTER
	if( cust->eventreq )
		DeleteIORequest( cust->eventreq );
	if( cust->eventport )
		DeleteMsgPort( cust->eventport );
	cust->eventport = NULL;
	cust->eventreq = NULL;
#endif
	return 0;
}

#ifdef GET_TICKS_FILTER
/* filter function that signals us for intuition timers */
struct InputXpression my_inputfilter =
{
 IX_VERSION, IECLASS_TIMER,
 0,0,
 (IEQUALIFIER_LEFTBUTTON|IEQUALIFIER_MIDBUTTON|IEQUALIFIER_RBUTTON),
 0
};

LONG cx_AddEventFilter( CxObj *cx_Broker, struct Task *self, ULONG signal )
{
	LONG ret = 1;

	CxObj *filterlist;

	if( (filterlist = CxFilter(NULL)) )
	{
		SetFilterIX( filterlist, &my_inputfilter );
		AttachCxObj( filterlist, CxSignal( self, signal ) );
		ret = 0;
	}

	AttachCxObj(cx_Broker,filterlist); 

	return ret;
}

/* look into joystick port */
LONG cx_Handle_Signal( struct cx_Custom *cust )
{
	UWORD joy1dat;
	ULONG code;

	joy1dat = *( (UWORD*)JOY1DAT ); /* last content of JOY1DAT */
	if( joy1dat ^ cust->lastXY )
	{
		LONG jdx,jdy;

		jdx = (joy1dat&0xff) - (cust->lastXY&0xff);
		jdy = (joy1dat>>8) - (cust->lastXY>>8);

		cust->lastXY = joy1dat;

		if( jdx )
		{
			if( jdx > 127 )
				jdx-=256;
			else if( jdx < -128 )
			 	jdx+=256;

			if( cust->swapX )
				jdx = -jdx;

			code = NM_WHEEL_RIGHT;
			if( jdx < 0 )
			{
				code = NM_WHEEL_LEFT;
				jdx  = -jdx;
			}
			sendEvents( cust, code, jdx );
		}

		if( jdy )
		{
			if( jdy > 127 )
				jdy-=256;
			else if( jdy < -128 )
				jdy+=256;

			if( cust->swapY )
				jdy = -jdy;

			code = NM_WHEEL_DOWN;
			if( jdy < 0 )
			{
				code = NM_WHEEL_UP;
				jdy  = -jdy;
			}
			sendEvents( cust, code, jdy );
		}

	}

	return 0;
}

void oneEvent( struct cx_Custom *cust, ULONG code, ULONG class )
{
	struct IOStdReq *r = cust->eventreq;

	r->io_Data    = (APTR)&cust->evt;
	r->io_Length  = sizeof(struct InputEvent);
	r->io_Command = IND_WRITEEVENT;

	cust->evt.ie_Class     = class;
	cust->evt.ie_Code      = code;
	cust->evt.ie_Qualifier = NULL;
	cust->evt.ie_NextEvent = NULL;

	DoIO( (struct IORequest*)r );
}

void sendEvents( struct cx_Custom *cust, ULONG code, ULONG count )
{
	while(count--)
	{
		oneEvent( cust, code, IECLASS_RAWKEY );
		oneEvent( cust, code, IECLASS_NEWMOUSE );
	}
}
#endif


LONG My_Int2Str( LONG val, STRPTR buf, LONG maxlen, LONG trailers )
{
	STRPTR b = buf;
	LONG a,c,t;

	a = 0;
	if( maxlen < 0 ) /* length <0 -> leading zeros */
	{
		a=1; /* output leading zeros */
		maxlen = -maxlen;
	}
	if( maxlen == 0 )
		maxlen = MAXLEN;

	if( val < 0 )
	{
		val = -val;
		*b++ = '-';
	}

	c=1000000000;
	while( ((b-buf) < maxlen) && (c>0) )
	{
		t = UDivMod32( val, c );
		if( (t) || (a) )
		{
			*b++ = t+'0';
			a=1;
		}
		t = UMult32( t, c );
		val -= t;
		c = UDivMod32( c, 10 );
		if( c < trailers )
			a=1;
	}

	return (LONG)(b-buf);
}

/* custom function that calls i2csensors.library and pushes the results to the external display */
LONG SendDataToI2C( struct configvars *conf )
{
	APTR *r;
	STRPTR s;
	SHORT i,count;
	ULONG arg=0;
	/* UBYTE i2caddr = (conf->i2caddr) ? *conf->i2caddr : I2C_DEF; */
	UBYTE sendbuf[MAXLEN+7],state,c;

	for( i=0, r = &conf->row0 ; i < NROWS ; i++, r++ )
	{
	 if( (s=*r) )
	 {
		count=0;
		/* set cursor (0x00..0x10) */
		ssd1306_setCursor( disp, 0, i<<3 );

#define S_IDLE 0
#define S_TEMPLATE_START 1
#define S_TEMPLATE_ARG2  2
#define S_TEMPLATE_ARG2N 3
#define S_TEMPLATE_MEMORY_ARG2 4

		/* put characters from template string to display */
		state = S_IDLE;
		while( (count < MAXLEN) && (c = *s++) )
		{
		 switch( state )
		 {
		 	/* numeric 2nd argument */
			case S_TEMPLATE_ARG2N:
				if( (c>='0') && (c<='9') )
				{
					arg |= (c-'0');
				}
				if( (c>='A') && (c<='F') )
				{
					arg  |= (c-'A'+0xa);
				}
				if( (c>='a') && (c<='f') )
				{
					arg |= (c-'a'+0xa);
				}
				// sendbuf[0]=arg;
				ssd1306_putChar( disp, arg ); // i2c_Send(i2caddr, 1, sendbuf );
				count++;
				state=S_IDLE;
				break;
			case S_TEMPLATE_MEMORY_ARG2:
				/* chip/fast RAM */
				arg = MEMF_TOTAL;
				if( c=='c' )
					arg = MEMF_CHIP;
				if( c=='C' )
					arg = MEMF_CHIP|MEMF_LARGEST;
				if( c=='f' )
					arg = MEMF_FAST;
				if( c=='F' )
					arg = MEMF_FAST|MEMF_LARGEST;
				{
					LONG l,val;

					val = AvailMem( arg );
					val = (val+15)>>4; /* >>20<<16, i.e. as 16.16 in Megabytes */

					/* rounding, dependend on value */
					if( val >= 65536*100L )	/* >100 = decimals only */
						val += 32768; /* + 0.5 */
					else
					{
						/* < 100 */
						if( val >= 65536L*10L ) /* >= 10: 1 fractional place */
							val += 3276; /* >= 10: 1 fractional place    */
						else	val += 327;  /* def: two fractional places   */
					}
					if( ((val>>16) >= 100) && (val>>16)<1000 )
					 ssd1306_putChar( disp, 32 );

					l = My_Int2Str( val>>16, sendbuf, MAXLEN - count, 10 );
					sendbuf[l]=0;
					ssd1306_putString( disp, sendbuf ); //i2c_Send( i2caddr, l, sendbuf );
					count+=l;
					/* values > 100 are output as decimals only */
					if( val < 65536L*100L )
					{
						LONG div;
						
					    sendbuf[0]='.';
					    ssd1306_putChar( disp, '.' );//i2c_Send( i2caddr, 1, sendbuf );
					    count++;

					    if( val < 65536L*10L )
					    		div=100;
					    else	div=10;
					    val = UMult32(val&65535,div);
					    //val += 32768; /* round, see above */
					    val >>= 16;
					    l = My_Int2Str( val, sendbuf, MAXLEN - count, div );
					    sendbuf[l]=0;
					    ssd1306_putString( disp, sendbuf );//i2c_Send( i2caddr, l, sendbuf );
					    count += l;
					}
				}
				state=S_IDLE;
				break;
			/* second argument */
			case S_TEMPLATE_ARG2:
				state=S_IDLE;
				if( (c>='0') && (c<='9') && (arg>=I2C_VOLTAGE) && (arg<=I2C_HUMIDITY) )
					c=(c-'0');
				else	
					break;
				{
					BYTE *unit,*name;
					LONG val = i2c_ReadSensor( arg, c, &unit, &name); 
					LONG l;
					if( val < 0 )
					{
					 //sendbuf[0]='-';
					 val=-val;
					 ssd1306_putChar( disp, '-' );// i2c_Send(i2caddr, 1, sendbuf );
					 count++;
					}

					/* rounding, dependend on value */
					if( val >= 65536*100L )	/* >100 = decimals only */
						val += 32768; /* + 0.5 */
					else
					{
						/* < 100 */
						if( val >= 65536L*10L ) /* >= 10: 1 fractional place */
							val += 3276; /* >= 10: 1 fractional place    */
						else	val += 327;  /* def: two fractional places   */
					}

					if( ((val>>16) >= 100) && (val>>16)<1000 )
					 ssd1306_putChar( disp, 32 );

					l = My_Int2Str( val>>16, sendbuf, MAXLEN - count, 10 );
					sendbuf[l]=0;
					ssd1306_putString( disp, sendbuf ); //i2c_Send( i2caddr, l, sendbuf );
					count+=l;
					/* values > 100 are output as decimals only */
					if( val < 65536L*100L )
					{
						LONG div;
						
					    sendbuf[0]='.';
					    ssd1306_putChar( disp, '.' );//i2c_Send( i2caddr, 1, sendbuf );
					    count++;

					    if( val < 65536L*10L )
					    		div=100;
					    else	div=10;
					    val = UMult32(val&65535,div);
					    //val += 32768; /* round, see above */
					    val >>= 16;
					    l = My_Int2Str( val, sendbuf, MAXLEN - count, div );
					    sendbuf[l]=0;
					    ssd1306_putString( disp, sendbuf );//i2c_Send( i2caddr, l, sendbuf );
					    count += l;
					}
				}
				break;
			case S_TEMPLATE_START:
				if( c=='%' )
				{
					//sendbuf[0]=c;
					ssd1306_putChar( disp, c );//i2c_Send(i2caddr, 1, sendbuf );
					count++;
					state=S_IDLE;
					break;
				}
				if( (c>='0') && (c<='9') )
				{
				 	state=S_TEMPLATE_ARG2N;
					arg = (c-'0')<<4;
					break;
				}
#if 0
				/* disabled: collides with F,C below */
				if( (c>='A') && (c<='F') )
				{
				 	state=S_TEMPLATE_ARG2N;
					arg = (c-'A'+0xa)<<4;
					break;
				}
#endif
				if( (c>='a') && (c<='f') )
				{
				 	state=S_TEMPLATE_ARG2N;
					arg = (c-'a'+0xa)<<4;
					break;
				}
				/* Keys: "T" for Temp, "V" for Voltage,
				         "F" for fan speed, "C" for current
					 "P" for pressure, "H" for humidity
				*/
				if( (c=='T') || (c=='t') )
				{
				 	state=S_TEMPLATE_ARG2;
					arg = I2C_TEMP;
					break;
				}
				if( (c=='V') || (c=='v') )
				{
				 	state=S_TEMPLATE_ARG2;
					arg = I2C_VOLTAGE;
					break;
				}
				if( (c=='F') )
				{
				 	state=S_TEMPLATE_ARG2;
					arg = I2C_FAN;
					break;
				}
				if( (c=='C') )
				{
				 	state=S_TEMPLATE_ARG2;
					arg = I2C_CURRENT;
					break;
				}
				if( (c=='P') || (c=='p') )
				{
				 	state=S_TEMPLATE_ARG2;
					arg = I2C_PRESSURE;
					break;
				}
				if( (c=='H') )
				{
				 	state=S_TEMPLATE_ARG2;
					arg = I2C_HUMIDITY;
					break;
				}
				if( (c=='M') )
				{
					state=S_TEMPLATE_MEMORY_ARG2;
				}
				break;
		 	default:
			case S_IDLE:
				if( c== '%' )
					state = S_TEMPLATE_START;
				else
				{
					//sendbuf[0]=c;
					ssd1306_putChar( disp, c );//i2c_Send(i2caddr, 1, sendbuf );
					count++;
				}
				break;
		 }

/*		 count++; */
		}

	 }
	}

//	sendbuf[0]=0x1f;
//	i2c_Send(i2caddr, 1, sendbuf );
	ssd1306_updateDisplay( disp );

	return 0;
}

