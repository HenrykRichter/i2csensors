#ifndef _INC_SSD1306_H
#define _INC_SSD1306_H

#include <exec/types.h>

struct ssd1306disp {
	UBYTE	i2c_address;
	UBYTE   cursor_x;
	UBYTE   cursor_y;
	UBYTE   unused;
	UBYTE	*framebuffer;
};


struct ssd1306disp*ssd1306_init( UBYTE _i2c_address );
void ssd1306_end( struct ssd1306disp* disp );

void ssd1306_setCursor( struct ssd1306disp* disp, UBYTE x, UBYTE y );
void ssd1306_putChar( struct ssd1306disp* disp, UBYTE c );
void ssd1306_putString( struct ssd1306disp* disp, UBYTE *str );

void ssd1306_putImg( struct ssd1306disp* disp, UBYTE*img, UBYTE w, UBYTE h );

void ssd1306_clear( struct ssd1306disp* disp );
void ssd1306_updateDisplay( struct ssd1306disp* disp );


void ssd1306_cmd(UBYTE i2caddr, UBYTE c);
void ssd1306_databyte(UBYTE i2caddr, UBYTE c);


#define SSD1306_WIDTH 128
#define SSD1306_HEIGHT 64
#define SSD1306_I2C_ADDRESS_DEFAULT 0x78

/* command mode parameters */
#define SSD1306_CHARGEPUMP 0x8D
#define SSD1306_COLUMNADDR 0x21
#define SSD1306_COMSCANDEC 0xC8
#define SSD1306_COMSCANINC 0xC0
#define SSD1306_DISPLAYALLON 0xA5
#define SSD1306_DISPLAYALLON_RESUME 0xA4
#define SSD1306_DISPLAYOFF 0xAE
#define SSD1306_DISPLAYON 0xAF
#define SSD1306_EXTERNALVCC 0x1
#define SSD1306_INVERTDISPLAY 0xA7
#define SSD1306_MEMORYMODE 0x20
#define SSD1306_NORMALDISPLAY 0xA6
#define SSD1306_PAGEADDR 0x22
#define SSD1306_SEGREMAP 0xA0
#define SSD1306_SETCOMPINS 0xDA
#define SSD1306_SETCONTRAST 0x81
#define SSD1306_SETDISPLAYCLOCKDIV 0xD5
#define SSD1306_SETDISPLAYOFFSET 0xD3
#define SSD1306_SETHIGHCOLUMN 0x10
#define SSD1306_SETLOWCOLUMN 0x00
#define SSD1306_SETMULTIPLEX 0xA8
#define SSD1306_SETPRECHARGE 0xD9
#define SSD1306_SETSEGMENTREMAP 0xA1
#define SSD1306_SETSTARTLINE 0x40
#define SSD1306_SETVCOMDETECT 0xDB
#define SSD1306_SWITCHCAPVCC 0x2



#endif /* _INC_SSD1306_H */
