/*
  version.h

  (C) 2020 Henryk Richter <henryk.richter@gmx.net>

  version and date handling
*/
#ifndef _INC_VERSION_H
#define _INC_VERSION_H

#define PROGNAME "OLEDSensors"
#define LIBVERSION  "1"
#define LIBREVISION "0"
/* #define DEVICEEXTRA Beta */
#define LIBDATE     "11.09.20"

#endif

