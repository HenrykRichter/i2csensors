/*
  config.c

  (C)2020 Henryk Richter <henryk.richter@gmx.net>

  purpose:
   configuration structure and reference to config strings

*/
#include <exec/types.h>
#include "config.h"

/* Important: apply changes to both confstringCLI and confvarsWB, also don't forget to
   adjust struct configvars accordingly as that struct is the direct result of a call
   to ReadArgs() */
STRPTR confstringCLI =  (STRPTR)"PRIORITY/K/N,QUIET/S,INTERVAL/K/N,I2CADDR/K/N,ROW0/K,ROW1/K,ROW2/K,ROW3/K,ROW4/K,ROW5/K,ROW6/K,ROW7/K";

/* every item here should shadow the position and type in confstringCLI */
struct configttitem confvarsWB[] = {
 {  (STRPTR)"PRIORITY",  CTTI_INT    },
 {  (STRPTR)"QUIET",     CTTI_SWITCH },
 {  (STRPTR)"INTERVAL",  CTTI_INT    },
 {  (STRPTR)"I2CADDR",   CTTI_INT    },
 {  (STRPTR)"ROW0",      CTTI_STRING },
 {  (STRPTR)"ROW1",      CTTI_STRING },
 {  (STRPTR)"ROW2",      CTTI_STRING },
 {  (STRPTR)"ROW3",      CTTI_STRING },
 {  (STRPTR)"ROW4",      CTTI_STRING },
 {  (STRPTR)"ROW5",      CTTI_STRING },
 {  (STRPTR)"ROW6",      CTTI_STRING },
 {  (STRPTR)"ROW7",      CTTI_STRING },
 { NULL, 0 }
};


