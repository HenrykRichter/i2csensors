/*
  Framebuffer and Text on OLED with SSD1306 controller
  

*/
#include <exec/types.h>
#include <exec/memory.h>

#include <proto/exec.h>
#include <proto/i2csensors.h>

#include <libraries/i2csensors.h>
#include "ssd1306.h"
#include "topaz8x8vertical.h"

/* I2C Library referenced externally */
extern struct Library *I2CSensorsBase;

void  ssd1306_cmd(UBYTE i2caddr, UBYTE c)
{
	UBYTE buf[2];
	buf[0] = 0x80;
	buf[1] = c;
	i2c_Send( i2caddr, 2, buf );
}

void  ssd1306_databyte(UBYTE i2caddr, UBYTE c)
{
	UBYTE buf[2];
	buf[0] = 0x40;
	buf[1] = c;
	i2c_Send( i2caddr, 2, buf );
}

struct ssd1306disp*ssd1306_init( UBYTE _i2c_address )
{
	struct ssd1306disp* ret;

	ret = (struct ssd1306disp*)AllocVec( sizeof(struct ssd1306disp) + (SSD1306_WIDTH * SSD1306_HEIGHT / 8) , MEMF_ANY );
	if( !ret )
		return ret;
	ret->framebuffer = (UBYTE*)(ret+1);

	if( _i2c_address != 0 )
		ret->i2c_address = _i2c_address;
	else	ret->i2c_address = SSD1306_I2C_ADDRESS_DEFAULT;

	ssd1306_cmd(ret->i2c_address,SSD1306_DISPLAYOFF);                    /* 0xAE*/
	ssd1306_cmd(ret->i2c_address,SSD1306_SETDISPLAYCLOCKDIV);            /* 0xD5*/
	ssd1306_cmd(ret->i2c_address,0x80); /* by datasheet */
	ssd1306_cmd(ret->i2c_address,SSD1306_SETMULTIPLEX);                  /* 0xA8*/
	ssd1306_cmd(ret->i2c_address,0x3F);
	ssd1306_cmd(ret->i2c_address,SSD1306_SETDISPLAYOFFSET);              /* 0xD3*/
	ssd1306_cmd(ret->i2c_address,0x1);                                   /* no offset*/
	ssd1306_cmd(ret->i2c_address,SSD1306_SETSTARTLINE);	/* line #0*/
	ssd1306_cmd(ret->i2c_address,0x0);                                   /* no offset*/
	ssd1306_cmd(ret->i2c_address,SSD1306_CHARGEPUMP);                    /* 0x8D*/
	ssd1306_cmd(ret->i2c_address,0x14);                                  /* using internal VCC*/
#if 0
	ssd1306_cmd(ret->i2c_address,SSD1306_MEMORYMODE);                    /* 0x20*/
	ssd1306_cmd(ret->i2c_address,0x00);                                  /* 0x00 horizontal addressing*/
	ssd1306_cmd(ret->i2c_address,SSD1306_SEGREMAP | 0x1);                /* rotate screen 180*/
	ssd1306_cmd(ret->i2c_address,SSD1306_COMSCANDEC);                    /* rotate screen 180*/
#endif
	ssd1306_cmd(ret->i2c_address,SSD1306_SETCOMPINS);                    /* 0xDA*/
	ssd1306_cmd(ret->i2c_address,0x12);
	ssd1306_cmd(ret->i2c_address,SSD1306_SETCONTRAST);                   /* 0x81*/
	ssd1306_cmd(ret->i2c_address,0xCF);
	ssd1306_cmd(ret->i2c_address,SSD1306_SETPRECHARGE);                  /* 0xd9*/
	ssd1306_cmd(ret->i2c_address,0xF1);
	ssd1306_cmd(ret->i2c_address,SSD1306_SETVCOMDETECT);                 /* 0xDB*/
	ssd1306_cmd(ret->i2c_address,0x40);
	ssd1306_cmd(ret->i2c_address,SSD1306_DISPLAYALLON_RESUME);           /* 0xA4*/
	ssd1306_cmd(ret->i2c_address,SSD1306_NORMALDISPLAY);                 /* 0xA6*/

	ssd1306_cmd(ret->i2c_address,SSD1306_DISPLAYON);                     /* switch on OLED*/

	ssd1306_clear( ret );
	ssd1306_updateDisplay( ret );

	return ret;
}

void ssd1306_end( struct ssd1306disp* disp )
{
	if( !disp )
		return;

	ssd1306_clear( disp );
	ssd1306_updateDisplay( disp );

	ssd1306_cmd(disp->i2c_address,SSD1306_DISPLAYOFF);     /* 0xAE*/

	FreeVec(disp);
}

void ssd1306_updateDisplay( struct ssd1306disp* disp )
{
	UWORD i,j;
	UBYTE *fb = disp->framebuffer;

/*	ssd1306_cmd(disp->i2c_address,SSD1306_DISPLAYON);*/

        ssd1306_cmd(disp->i2c_address,SSD1306_COLUMNADDR);
        ssd1306_cmd(disp->i2c_address,0);
        ssd1306_cmd(disp->i2c_address,SSD1306_WIDTH-1);

        ssd1306_cmd(disp->i2c_address,SSD1306_PAGEADDR);
        ssd1306_cmd(disp->i2c_address,0); /* start page */
        ssd1306_cmd(disp->i2c_address,7); /* end page: 7 for 128x64, would be 3 for 128x32 */

	for( j=0 ; j < (SSD1306_HEIGHT/8) ; j++ )
	{
	        ssd1306_cmd(disp->i2c_address,SSD1306_COLUMNADDR);
	        ssd1306_cmd(disp->i2c_address,0);
	        ssd1306_cmd(disp->i2c_address,SSD1306_WIDTH-1);

	        ssd1306_cmd(disp->i2c_address,SSD1306_PAGEADDR);
	        ssd1306_cmd(disp->i2c_address,j); /* start page */
	/*	ssd1306_cmd(disp->i2c_address,7);*/ /* end page: 7 for 128x64, would be 3 for 128x32 */
	
		for( i=0 ; i < SSD1306_WIDTH ; i++ )
		{
			ssd1306_databyte( disp->i2c_address, *fb++ );
		}
	}
	
}

/* put cursor on given row/col */
/* row is rounded down to a multiple of 8 */
void ssd1306_setCursor( struct ssd1306disp* disp, UBYTE x, UBYTE y )
{
	if( x >= SSD1306_WIDTH )
		x=SSD1306_WIDTH-1;
	if( y >= SSD1306_HEIGHT )
		x=SSD1306_HEIGHT-1;

	disp->cursor_x = x;
	disp->cursor_y = y>>3;
}

void ssd1306_putChar( struct ssd1306disp* disp, UBYTE c )
{
	UWORD i;
	ULONG loc = (ULONG)disp->cursor_x + (ULONG)disp->cursor_y * SSD1306_WIDTH;
	UBYTE *fb = disp->framebuffer + loc;
	UBYTE *charbuf = (UBYTE*)waveshare_r_image;
	
	if( c<32 )
		c=32;
	charbuf += ((ULONG)(c-32))<<3;
	
	for( i=0 ; i < 8 ; i++ )
	 *fb++ = *charbuf++;

	/* cursor update */
	disp->cursor_x += 8;
	if( disp->cursor_x >= SSD1306_WIDTH ) /* wraparound */
		disp->cursor_x = 0;
}

void ssd1306_putString( struct ssd1306disp* disp, UBYTE *str )
{
 while( *str != 0 )
 {
  ssd1306_putChar( disp, *str );
  str++;
 }
}

void ssd1306_putImg( struct ssd1306disp* disp, UBYTE*img, UBYTE w, UBYTE h )
{
	/* TODO */
}

void ssd1306_clear( struct ssd1306disp* disp )
{
	UWORD i;
	UBYTE *fb = disp->framebuffer;

	for( i=0 ; i < (SSD1306_WIDTH * SSD1306_HEIGHT / 8) ; i++ )
	 *fb++ = 0;

}


