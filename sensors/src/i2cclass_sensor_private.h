#ifndef _INC_I2CCLASS_SENSOR_PRIVATE_H
#define _INC_I2CCLASS_SENSOR_PRIVATE_H

#define CUSTOMFUNC(_bla_) LONG (*_bla_)(struct i2c_sensorbase *,struct i2s_sensor*)

union flong {
	LONG    l;
	FRACNUM f;
};
/* map known sensor readings internally to a struct (PRIVATE!, subject to change) */
struct i2s_sensor {
	struct  MinNode n;
	BYTE	*name;     /* device name */
	BYTE	*unit;     /* unit "V","A","RPM",... */
	ULONG	type;	   /* */
	CUSTOMFUNC (cfunc);

	USHORT	i2caddr;     /* */
	UBYTE	unavailable; /* device is not responding, ignore */
	UBYTE	readbytes;   /* */
	UBYTE	bitoffset;   /* */
	UBYTE	numbits;     /* */
	LONG    signbit;     /* */
	ULONG	extractmask; /* */
	UBYTE	wakeup;
	UBYTE   usedivby;  /* calculate mult/val instead of mult*val if this is set */
	UBYTE	custom_b0; /* space for custom sensors */
	UBYTE   custom_b1;
	ULONG   custom_l0;
	ULONG   custom_l1; 
	UBYTE*	readpre;   /* */
	ULONG   id;        /* V2: sensor ID */
	union flong mult;   /* either 16.16 FIX or SP float */
	union flong add;    /* either 16.16 FIX or SP float */
	union flong calib1; /* either 16.16 FIX or SP float */
	union flong calib2; /* either 16.16 FIX or SP float */
};

#endif /* _INC_I2CCLASS_SENSOR_PRIVATE_H */
