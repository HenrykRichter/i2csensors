/*
  file:   custom_zz9000.h

  proposal for I2C class for sensor reading

  author:  Henryk Richter <henryk.richter@gmx.net>

  concept: 
          
           Custom sensors are defined in three places:
           i2cclass_sensor.h
            definition of custom sensor Tag name
             I2SENC_*
           config.c
            definition of keyword to Tag name
             struct keytag confcustomtag[] = {}
           i2cclass_sensor.c
            function call, see "case I2SEN_CUSTOM:"
  
*/
#ifndef _INC_CUSTOM_ZZ9000_H
#define _INC_CUSTOM_ZZ9000_H

#include "macros.h"
#include "config.h"
#include "i2cclass_sensor.h"
#include "debug.h"

LONG SAVEDS custom_ZZ9000TEMP(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s);
LONG SAVEDS custom_ZZ9000VOLT(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s);

#endif /* _INC_CUSTOM_ZZ9000_H */
