/*
  file:   custom_zz9000.c

  Custom functions for Z9000 voltage/temp reading

  author:  Henryk Richter <henryk.richter@gmx.net>

  concept: 
           Custom sensors are defined in three places:
           i2cclass_sensor.h
            definition of custom sensor Tag name
             I2SENC_*
           config.c
            definition of keyword to Tag name
             struct keytag confcustomtag[] = {}
           i2cclass_sensor.c
            function call, see "case I2SEN_CUSTOM:"
  
*/
#include <exec/memory.h>
#include <proto/exec.h>
#include <proto/utility.h>
#include <proto/dos.h>
#include <proto/timer.h>
#include <dos/exall.h>
#include <proto/expansion.h>
#include <libraries/configvars.h>

#define __CONSTLIBBASEDECL__ /* some GCC targeted includes carry this attribute */ 
#include "zz9000.h"

#include "macros.h"
#include "config.h"
/* _I2CCLASS_INTERNAL triggers redirected library pointers 
   (DOSBase,I2CBase,UtilityBase etc.)
*/
#define  _I2CCLASS_INTERNAL
#include "i2cclass_sensor.h"
#include "debug.h"

#include "i2cclass_sensor_private.h"

/* local defs */
#define ZZBASE custom_l0


ULONG find_zz( struct i2c_sensorbase *i2c_sensorbase )
{	
	ULONG ret = 0;
	struct ConfigDev *zzdev;
	struct Library *ExpansionBase = OpenLibrary( (STRPTR)"expansion.library", 36 );
	if( !ExpansionBase )
		return 0;

	zzdev = (struct ConfigDev*)FindConfigDev(NULL,0x6d6e,0x3);
	if( !zzdev )
		zzdev = (struct ConfigDev*)FindConfigDev(NULL,0x6d6e,0x4);
	if( zzdev )
	{
		ret = (ULONG)(zzdev->cd_BoardAddr);
	}

	CloseLibrary(ExpansionBase);
	
	return ret;
}

UBYTE *get_zz(  struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s )
{
	if( !s->ZZBASE )
	{
		s->ZZBASE = find_zz(i2c_sensorbase);
	}
	return (UBYTE*)s->ZZBASE;
}


UWORD zz_read_reg(ULONG offset, UBYTE*regbase )
{
  return *((volatile UWORD*)(regbase+offset));
}

/*
   PRIVATE
   custom temperature reading code for Z9000 graphics card 
*/
LONG SAVEDS custom_ZZ9000TEMP(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s)
{
	UBYTE *regbase;
	ULONG T;
	
	if( !(regbase = get_zz( i2c_sensorbase, s )) )
		return 0;

	/* Celsius * 10 */
	T = zz_read_reg(REG_ZZ_TEMPERATURE,regbase);
	T = UDivMod32( UMult32( T, 65536 )+5, 10 );	

	return (LONG)T;
}

LONG SAVEDS custom_ZZ9000VOLT(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s)
{
	UBYTE *regbase;
	ULONG V;

	if( !(regbase = get_zz( i2c_sensorbase, s )) )
		return 0;
	
	/* Voltage * 100 */
	V = zz_read_reg(REG_ZZ_VOLTAGE_AUX,regbase);
	V = UDivMod32( UMult32( V, 65536 )+50, 100 );	

	return (LONG)V;
}


