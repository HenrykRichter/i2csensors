#ifndef _PROTO_I2CSENSORS_H
#define _PROTO_I2CSENSORS_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif
#if !defined(CLIB_I2CSENSORS_PROTOS_H) && !defined(__GNUC__)
#include <clib/i2csensors_protos.h>
#endif

#ifndef __NOLIBBASE__
extern void *I2CSensorsBase;
#endif

#ifdef __GNUC__
#ifdef __AROS__
#include <defines/i2csensors.h>
#else
#include <inline/i2csensors.h>
#endif
#elif defined(__VBCC__)
#include <inline/i2csensors_protos.h>
#else
#include <pragma/i2csensors_lib.h>
#endif

#endif	/*  _PROTO_I2CSENSORS_H  */
