/* Automatically generated header (sfdc 1.11e)! Do not edit! */

#ifndef CLIB_I2CSENSORS_PROTOS_H
#define CLIB_I2CSENSORS_PROTOS_H

/*
**   $VER: i2csensors_protos.h $Id: i2csensors_lib.sfd $ $Id: i2csensors_lib.sfd $
**
**   C prototypes. For use with 32 bit integers only.
**
**   Copyright (c) 2001 Amiga, Inc.
**       All Rights Reserved
*/

#include <exec/types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

LONG i2c_SensorNum(LONG type);
LONG i2c_ReadSensor(LONG type, LONG index, BYTE * *unitstring, BYTE * *namestring);
LONG i2c_Obtain(LONG i2cAddress);
LONG i2c_Release(LONG i2cAddress);
ULONG i2c_Send(UBYTE i2caddr, UWORD nbytes, UBYTE * senddata);
ULONG i2c_Receive(UBYTE i2caddr, UWORD nbytes, UBYTE * recvdata);
ULONG i2c_SensorID(LONG type, LONG index);
LONG i2c_SensorTypeFromID(ULONG ID, LONG * index);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CLIB_I2CSENSORS_PROTOS_H */
