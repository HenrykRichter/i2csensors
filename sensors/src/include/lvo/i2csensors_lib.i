* Automatically generated header (sfdc 1.11e)! Do not edit!
     IFND  LVO_I2CSENSORS_LIB_I
LVO_I2CSENSORS_LIB_I   SET   1

_LVOi2c_SensorNum     EQU   -30 
_LVOi2c_ReadSensor     EQU   -36
_LVOi2c_Obtain     EQU   -42
_LVOi2c_Release     EQU   -48
_LVOi2c_Send     EQU   -54
_LVOi2c_Receive     EQU   -60
_LVOi2c_SensorID     EQU   -66
_LVOi2c_SensorTypeFromID     EQU   -72

     ENDC  * LVO_I2CSENSORS_LIB_I
