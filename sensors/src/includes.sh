#!/bin/sh
#
#cp i2csensors_lib.sfd include/sfd/
#cp i2csensors.h include/i2csensors/
#cp i2csensors.i include/i2csensors/

# Convert SFD into the includes needed
sfdc include/sfd/i2csensors_lib.sfd --mode=clib -o include/clib/i2csensors_protos.h
sfdc include/sfd/i2csensors_lib.sfd --mode=macros -o include/inline/i2csensors.h
sfdc include/sfd/i2csensors_lib.sfd --mode=pragmas -o include/pragmas/i2csensors_pragmas.h
sfdc include/sfd/i2csensors_lib.sfd --mode=proto -o include/proto/i2csensors.h
sfdc include/sfd/i2csensors_lib.sfd --mode=lvo -o include/lvo/i2csensors_lib.i

#
cd include
fd2pragma -s 70 --voidbase -i fd/i2csensors_lib.fd --clib clib/i2csensors_protos.h --to vbcc/inline/
fd2pragma -s 38 --voidbase -i fd/i2csensors_lib.fd --clib clib/i2csensors_protos.h --to vbcc/proto/
#fd2pragma special 70 voidbase fd/i2csensors_lib.fd clib clib/i2csensors_protos.h to vbcc/inline/
#fd2pragma special 38 voidbase fd/i2csensors_lib.fd clib clib/i2csensors_protos.h to vbcc/proto/
cd ..
