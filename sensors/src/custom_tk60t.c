/*
  file:   custom_tk60t.c

  proposal for I2C class for sensor reading

  author:  Henryk Richter <henryk.richter@gmx.net>

  concept: This file contains the sensor reading and
           parsing functions for the LTC2990 on 
           the Matze A3000/4000 68040/68060 card. The
           temperature calculation requires a look at
           VCC to compensate the voltage reading over
           the temperature sensing resistor. Hence,
           custom code.
           
           Custom sensors are defined in three places:
           i2cclass_sensor.h
            definition of custom sensor Tag name
             I2SENC_*
           config.c
            definition of keyword to Tag name
             struct keytag confcustomtag[] = {}
           i2cclass_sensor.c
            function call, see "case I2SEN_CUSTOM:"
  
*/
#include <exec/memory.h>
#include <proto/exec.h>
#include <proto/utility.h>
#include <proto/dos.h>
#include <proto/timer.h>
#include <dos/exall.h>

#define __CONSTLIBBASEDECL__ /* some GCC targeted includes carry this attribute */ 
#include <proto/i2c.h>

#include "macros.h"
#include "config.h"
/* _I2CCLASS_INTERNAL triggers redirected library pointers 
   (DOSBase,I2CBase,UtilityBase etc.)
*/
#define  _I2CCLASS_INTERNAL
#include "i2cclass_sensor.h"
#include "debug.h"

#include "i2cclass_sensor_private.h"

/* if we get an error, return unavailable sensor */
#define CHKRES if( !(res & 0xf) ){ return I2RET_NOSENSOR;}

/*
   PRIVATE
   custom temperature reading code for Matze`s TK60

         R1        R2     Rx
   VCC--[1k]--*--[470R]--[Rx]--GND
              |
              V4
              
   Rx is 780 Ohm at 25�C (XC68060,MC68060)
   temperature coefficient 2.8 Ohm / �C
*/
#define R1 1000L
#define R2 470L

LONG SAVEDS custom_TK60TEMP(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s)
{
	LONG res;
	LONG VCC,V4,Vr,Rx8,Tx16,DIVID;
	UBYTE *p = (UBYTE*)&i2c_sensorbase->readbuffer[ 0 ];
	//BYTE* sp = (BYTE*)p; /* signed input */

	/* first: read VCC (we could also use the V1 sensor
	   on this card) but the VCC measurement is independent 
	   of the voltage divider resistors */
	p[0] = 0x0E;
	i2c_Send( i2c_sensorbase,  s->i2caddr, 1, p );
	res = i2c_Receive( i2c_sensorbase,  s->i2caddr, 2, p ); /* VCC */
	CHKRES
	VCC = (( (ULONG)(0x3F & p[0]) )<<8) + (ULONG)p[1];

	p[0] = 0x0C;
	i2c_Send( i2c_sensorbase,  s->i2caddr, 1, p );
	res = i2c_Receive( i2c_sensorbase,  s->i2caddr, 2, p ); /* V4 */
	CHKRES /* these errors are fatal: no comms == sensor unavailable (0) */
	V4 = (( (ULONG)(0x3F & p[0]) )<<8) + (ULONG)p[1];

/*
I2SEN_CALIB1
I2SEN_CALIB2
*/
	/* Raw VCC is in 305.18�V units, with an offset of 2.5V */
	/* V4 is also in 305.18�V units. Since we only need the ratio V4/VCC,
	   we don`t need to normalize. */
	VCC  += 0x1FFF; /* +2.5V = 8191*305.18�V    */

	D(("V4RAW %ld VCC %ld\n",V4,VCC));

	if( !VCC )	/* sanity: avoid /0 */
		/* res==0 -> sensor unavailable, res==1 -> no valid data (yet) */
		return I2RET_NODATA;

	V4  <<= 16;       /* get some fractional bits */
	V4   += (VCC>>1); /* rounding */
	Vr    = V4/VCC;   /* voltage ratio * 65536    */

	D(("Vr %ld\n",Vr));

	/* 
	   formula (unscaled, voltage divider solved for Rx)
	    Rx = (Vr`*(1000+470) - 470)/(1-Vr`)
	    
	   due to Vr scaling -> Rx normalized
	    Rx = (Vr*(1000+470) - (470<<16) ) / ( (1<<16) - Vr)

	   eight more bits in Rx
	    (Rx<<8) = ((Vr*(1000+470) - (470<<16) )<<4) / (((1<<16)-Vr)>>4)
	*/
	res  = 0; /* default: temp==0 */
	DIVID= (( (1<<16)-Vr )>>4);
	if( DIVID )
	{
		LONG Rref = 780;
		LONG Tref = 25;
		
		/* calibration considerations:
		    - spec sheet (actually errata) says 780 Ohm @ 25C
		    - actual CPUs are deviating in base resistance at 25C
		      but this thankfully is just an offset while the 
		      temperature coefficient is pretty much the same
		   see https://www.ppa.pl/forum/elektronika/40796/niskoprofilowy-regler-3v3-do-apollo-1260#m686316
		   (by Szymon Bieganski)
		   
		   recalculation:
		    - get Rcal and Tcal as calibration results
		    - assume constant 2.8 ohms/ degree C

		    re-compute: Rref = Rcal - 2.8*(25-Tcal)
		    to obtain Rref at 25C

		    or just re-use Rref=Rcal, Tref=Tcal since this whole
		    thing is linear
		*/
		Tref <<= 16;
		Rref <<= 16;
		if( (s->calib1.l != 0) && (s->calib2.l != 0) )
		{
#ifdef USE_MATH
		 Rref = FIX( FADD( FMUL( s->calib1.f , 65536.f ), 0.5f )); /* convert to FIX1616 */
		 Tref = FIX( FADD( FMUL( s->calib2.f , 65536.f ), 0.5f ));
#else
		 Rref = s->calib1;
		 Tref = s->calib2;
#endif
		}

		/* inserted: support for ClockIIC in addition to BFG9060 circuit
		             difference: ClockIIC doesn't have the 470R to keep voltages on LTC2990 V4 input smaller
			     -> different measurements but those curves don't overlap in the relevant range (-10...120�C),
			        i.e. with R2 goes from 2.69...3.01V (within -10...120�C)
				     without R2 goes from 2.05V ... 2.55V
		*/
		if( Vr < 34078 ) /* Vr=V4/VCC*65536, threshold is 2.6V@5V -> 2.6V/5V=0.52 -> 0.52*65536=34078 */
		{
			/* we don't have R2 on ClockIIC */
			Rx8  = (( Vr*R1 )<<4) / DIVID;
		}
		else
		{
			/* BFG9060 has R2==470 */
			Rx8  = (( Vr*(R1+R2) - (R2<<16) )<<4) / DIVID;
		}
		D(("Rx8 %ld Rx %ld\n",Rx8,(Rx8>>8) ));

		/* Rx8 should be around (780...1000)<<8 */
		Rx8<<= 11; /* (12 Bit )<<8<<11 = 31 Bit */

		/* this leaves 4 bit of fraction */
		Tx16   = ( Rx8-(Rref<<3)+45875 ) / 91750L; /* /(2.8<<15) */
		Tx16 <<= 12;   /* bolster to 16 bit fraction */
		Tx16  += Tref; /* Tref is already 16.16 */
		D(("Tx16 %ld Tx %ld\n",Tx16,(Tx16>>16) ));
		res = Tx16;
	}
	/* discard nonsensical results (i.e. no 060 on card or disconnected PTC) */
	if( (res > 150L<<16) || (res < -40L<<16) )
		res = I2RET_NODATA; /* res==0 -> sensor unavailable, res==1 -> no valid data (yet) */
		
	return res;
}

