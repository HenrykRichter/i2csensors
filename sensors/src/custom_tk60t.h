/*
  file:   custom_tk60t.h

  proposal for I2C class for sensor reading

  author:  Henryk Richter <henryk.richter@gmx.net>

  concept: This file contains the sensor reading and
           parsing functions for the LTC2990 on 
           the Matze A3000/4000 68040/68060 card. The
           temperature calculation requires a look at
           VCC to compensate the voltage reading over
           the temperature sensing resistor. Hence,
           custom code.
           
           Custom sensors are defined in three places:
           i2cclass_sensor.h
            definition of custom sensor Tag name
             I2SENC_*
           config.c
            definition of keyword to Tag name
             struct keytag confcustomtag[] = {}
           i2cclass_sensor.c
            function call, see "case I2SEN_CUSTOM:"
  
*/
#ifndef _INC_CUSTOM_TK60T_H
#define _INC_CUSTOM_TK60T_H

#include "macros.h"
#include "config.h"
#include "i2cclass_sensor.h"
#include "debug.h"

LONG SAVEDS custom_TK60TEMP(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s);

#endif /* _INC_CUSTOM_TK60T_H */
