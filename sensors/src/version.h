/*
  version.h

  (C) 2018 Henryk Richter <henryk.richter@gmx.net>

  version and date handling
*/
#ifndef _INC_VERSION_H
#define _INC_VERSION_H

#define LIBVERSION  2
#define LIBREVISION 9
#ifndef LIBEXTRA
#define LIBEXTRA
#endif
/* #define DEVICEEXTRA Beta */
#define LIBDATE     07.09.24

#endif
