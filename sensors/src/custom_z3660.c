/*
  file:   custom_Z3660.c

  Custom functions for Z3660 voltage/temp reading

  author:  Henryk Richter <henryk.richter@gmx.net>, Adapted from ZZ9000 to Z3660 by Adam Polkosnik <apolkosnik@gmail.com>

  concept: 
           Custom sensors are defined in three places:
           i2cclass_sensor.h
            definition of custom sensor Tag name
             I2SENC_*
           config.c
            definition of keyword to Tag name
             struct keytag confcustomtag[] = {}
           i2cclass_sensor.c
            function call, see "case I2SEN_CUSTOM:"
  
*/
#include <exec/memory.h>
#include <proto/exec.h>
#include <proto/utility.h>
#include <proto/dos.h>
#include <proto/timer.h>
#include <dos/exall.h>
#include <proto/expansion.h>
#include <libraries/configvars.h>

#define __CONSTLIBBASEDECL__ /* some GCC targeted includes carry this attribute */ 

/* zzregs would have naming collisions with zz9000.h */
#include "z3660.h"

#include "macros.h"
#include "config.h"
/* _I2CCLASS_INTERNAL triggers redirected library pointers 
   (DOSBase,I2CBase,UtilityBase etc.)
*/
#define  _I2CCLASS_INTERNAL
#include "i2cclass_sensor.h"
#include "debug.h"

#include "i2cclass_sensor_private.h"

/* local defs */
#define Z3660BASE custom_l0
#define Z3660_MAN_ID 0x144b
#define Z3660_PROD_ID_1 0x1
#define Z3660_PROD_ID_2 0x2

ULONG find_z3660( struct i2c_sensorbase *i2c_sensorbase )
{	

	ULONG ret = 0;
	struct ConfigDev* z3660_conf_dev;
	struct Library *ExpansionBase = OpenLibrary( (STRPTR)"expansion.library", 36 );
	if( !ExpansionBase )
		return 0;

	z3660_conf_dev = (struct ConfigDev*)FindConfigDev(NULL,Z3660_MAN_ID,Z3660_PROD_ID_1);

	if(z3660_conf_dev){
		ret = (ULONG)(z3660_conf_dev->cd_BoardAddr);
	}
	CloseLibrary(ExpansionBase);
	
	return ret;
}

UBYTE *get_z3660(  struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s )
{
	if( !s->Z3660BASE )
	{
		s->Z3660BASE = find_z3660(i2c_sensorbase);
	}
	return (UBYTE*)s->Z3660BASE;
}


UWORD z3660_read_reg(ULONG offset, UBYTE*regbase )
{
  return (UWORD)(*((volatile ULONG*)(regbase+offset)));
}

/*
   PRIVATE
   custom temperature reading code for Z9000 graphics card 
*/
LONG SAVEDS custom_Z3660_TEMPERATURE(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s)
{
	UBYTE *regbase;
	ULONG T;

	if( !(regbase = get_z3660( i2c_sensorbase, s )) )
		return 0;

	/* Celsius * 10 */
	T = z3660_read_reg(REG_Z3660_TEMPERATURE,regbase);
	T = UDivMod32( UMult32( T, 65536 )+5, 10 );

	return (LONG)T;
}

LONG SAVEDS custom_Z3660_LTC_TEMP(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s)
{
	UBYTE *regbase;
	ULONG T;

	if( !(regbase = get_z3660( i2c_sensorbase, s )) )
		return 0;

	/* Celsius * 10 */
	T = z3660_read_reg(REG_Z3660_LTC_TEMP,regbase);
	T = UDivMod32( UMult32( T, 65536 )+5, 100 );

	return (LONG)T;
}

LONG SAVEDS custom_Z3660_LTC_060_TEMP(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s)
{
	UBYTE *regbase;
	ULONG T;

	if( !(regbase = get_z3660( i2c_sensorbase, s )) )
		return 0;

	/* Celsius * 10 */
	T = z3660_read_reg(REG_Z3660_LTC_060_TEMP,regbase);
	T = UDivMod32( UMult32( T, 65536 )+5, 100 );

	return (LONG)T;
}

LONG SAVEDS custom_Z3660_VOLTAGE_AUX(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s)
{
	UBYTE *regbase;
	ULONG V;

	if( !(regbase = get_z3660( i2c_sensorbase, s )) )
		return 0;
	
	/* Voltage * 100 */
	V = z3660_read_reg(REG_Z3660_VOLTAGE_AUX,regbase);
	V = UDivMod32( UMult32( V, 65536 )+50, 100 );
	return (LONG)V;
}

LONG SAVEDS custom_Z3660_VOLTAGE_INT(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s)
{
	UBYTE *regbase;
	ULONG V;

	if( !(regbase = get_z3660( i2c_sensorbase, s )) )
		return 0;

	/* Voltage * 100 */
	V = z3660_read_reg(REG_Z3660_VOLTAGE_INT,regbase);
	V = UDivMod32( UMult32( V, 65536 )+50, 100 );
	return (LONG)V;
}

LONG SAVEDS custom_Z3660_LTC_V1(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s)
{
	UBYTE *regbase;
	ULONG V;

	if( !(regbase = get_z3660( i2c_sensorbase, s )) )
		return 0;

	/* Voltage * 100 */
	V = z3660_read_reg(REG_Z3660_LTC_V1,regbase);
	V = UDivMod32( UMult32( V, 65536 )+50, 100 );
	return (LONG)V;
}

LONG SAVEDS custom_Z3660_LTC_V2(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s)
{
	UBYTE *regbase;
	ULONG V;

	if( !(regbase = get_z3660( i2c_sensorbase, s )) )
		return 0;

	/* Voltage * 100 */
	V = z3660_read_reg(REG_Z3660_LTC_V2,regbase);
	 

	return (LONG)UDivMod32( UMult32( V, 65536 )+50, 100 );
}

LONG SAVEDS custom_Z3660_LTC_VCC(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s)
{
	UBYTE *regbase;
	ULONG V;

	if( !(regbase = get_z3660( i2c_sensorbase, s )) )
		return 0;

	/* Voltage * 100 */
	V = z3660_read_reg(REG_Z3660_LTC_VCC,regbase);
	V = UDivMod32( UMult32( V, 65536 )+50, 100 );

	return (LONG)V;
}
