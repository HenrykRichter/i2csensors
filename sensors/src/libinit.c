/*
  libinit.c 

  (C) 2018 Henryk Richter <henryk.richter@gmx.net>

  initialization structures and data

*/
#include <proto/exec.h>
#include <proto/utility.h>
#include <proto/dos.h>
#include <dos/dostags.h>
#include <utility/tagitem.h>
#include <exec/lists.h>
#include <exec/initializers.h>
#include "version.h"
#include "macros.h"
#include "config.h"
#include "i2cclass_sensor.h"

#define xstr(a) str(a)
#define str(a) #a

#ifndef LIBEXTRA
#define LIBEXTRA
#endif

#ifdef __SASC
#define _STATIC_ static
#else
#define _STATIC_
#endif
_STATIC_ const unsigned short hdr[] = { 0x2456,0x4552,0x3a20 }; /* "$VER: " */
const char *_LibVersionString = xstr(LIBNAME) " " xstr(LIBVERSION) "." xstr(LIBREVISION) " (" xstr(LIBDATE) ") " xstr(LIBEXTRA) "\r\n";
const char LibName[] = xstr(LIBNAME);

const APTR LibFunctions[] = {
	(APTR) LibOpen,
	(APTR) LibClose,
	(APTR) LibExpunge,
	(APTR) LibNull,
	(APTR) i2c_SensorNum,
	(APTR) i2c_ReadSensor,
	(APTR) i2c_Obtain,
	(APTR) i2c_Release,
	(APTR) i2c_Send,
	(APTR) i2c_Receive,
	(APTR) i2c_SensorID,         /* V2 */
	(APTR) i2c_SensorTypeFromID, /* V2 */
	(APTR) -1
};


#define WORDINIT(_a_) UWORD _a_ ##W1; UWORD _a_ ##W2; UWORD _a_ ##ARG;
#define LONGINIT(_a_) UBYTE _a_ ##A1; UBYTE _a_ ##A2; ULONG _a_ ##ARG;
struct LibInitData
{
	WORDINIT(w1) 
	LONGINIT(l1)
	WORDINIT(w2) 
	WORDINIT(w3) 
	WORDINIT(w4) 
	LONGINIT(l2)
	ULONG end_initlist;
} LibInitializers =
{
	INITBYTE(     STRUCTOFFSET( Node,  ln_Type),         NT_LIBRARY),
	0x80, (UBYTE) ((LONG)STRUCTOFFSET( Node,  ln_Name)), (ULONG) &LibName[0],
	INITBYTE(     STRUCTOFFSET(Library,lib_Flags),       LIBF_SUMUSED|LIBF_CHANGED ),
	INITWORD(     STRUCTOFFSET(Library,lib_Version),     LIBVERSION  ),
	INITWORD(     STRUCTOFFSET(Library,lib_Revision),    LIBREVISION ),
	0x80, (UBYTE) ((LONG)STRUCTOFFSET(Library,lib_IdString)), (ULONG) &_LibVersionString,
	(ULONG) 0
};


const APTR LibInitTab[] = {
	(APTR) sizeof( BASETYPE ),
	(APTR) &LibFunctions,
	(APTR) &LibInitializers,
	(APTR) LibInit
};

#if 0
float blah2(float a )
{
	return a;
}
#endif
