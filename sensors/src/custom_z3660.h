/*
  file:   custom_z3660.h

  proposal for I2C class for sensor reading

  author:  Henryk Richter <henryk.richter@gmx.net>, Adapted from ZZ9000 to Z3660 by Adam Polkosnik <apolkosnik@gmail.com>

  concept: 
          
           Custom sensors are defined in three places:
           i2cclass_sensor.h
            definition of custom sensor Tag name
             I2SENC_*
           config.c
            definition of keyword to Tag name
             struct keytag confcustomtag[] = {}
           i2cclass_sensor.c
            function call, see "case I2SEN_CUSTOM:"
  
*/
#ifndef _INC_CUSTOM_Z3660_H
#define _INC_CUSTOM_Z3660_H

#include "macros.h"
#include "config.h"
#include "i2cclass_sensor.h"
#include "debug.h"

LONG SAVEDS custom_Z3660_TEMPERATURE(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s);
LONG SAVEDS custom_Z3660_VOLTAGE_AUX(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s);
LONG SAVEDS custom_Z3660_VOLTAGE_INT(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s);
LONG SAVEDS custom_Z3660_LTC_TEMP(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s);
LONG SAVEDS custom_Z3660_LTC_V1(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s);
LONG SAVEDS custom_Z3660_LTC_V2(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s);
LONG SAVEDS custom_Z3660_LTC_060_TEMP(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s);
LONG SAVEDS custom_Z3660_LTC_VCC(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s);

#endif /* _INC_CUSTOM_Z3660_H */
